#  CONVERCIONES para UserDefaults
Almacenar el correo del usuario (Para la verificacion inicial de token)
UserDefaults.standard.string(forKey: "correo")
Almacenar el token de la cuenta (Para la verificacion inicial de token)
UserDefaults.standard.string(forKey: "token")
Almacena la información del usuario (apellidos, correo, nombre, token)
UserDefaults.standard.string(forKey: "UsuarioData")
