//
//  Usuario.swift
//  varaapp
//
//  Created by Missael Hernandez on 20/07/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
struct Informante : Codable{
    var id : Int?
    var nombre : String?
    var correoElectronico: String?
    var apellidoPaterno : String?
    var apellidoMaterno : String?
    var institucion : String?
    var telefonoMovil : String?
}
