//
//  FotografiaVaramiento+CoreDataProperties.swift
//  varaapp
//
//  Created by Missael Hernandez on 12/09/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//
//

import Foundation
import CoreData


extension FotografiaVaramiento {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FotografiaVaramiento> {
        return NSFetchRequest<FotografiaVaramiento>(entityName: "FotografiaVaramiento")
    }

    @NSManaged public var fotografia: NSData?
    @NSManaged public var varamiento: Varamiento?

}
