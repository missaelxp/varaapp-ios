//
//  Varamiento+CoreDataProperties.swift
//  varaapp
//
//  Created by Missael Hernandez on 12/09/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//
//

import Foundation
import CoreData


extension Varamiento {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Varamiento> {
        return NSFetchRequest<Varamiento>(entityName: "Varamiento")
    }

    @NSManaged public var acantilado: Bool
    @NSManaged public var coordenadas_latitud: Double
    @NSManaged public var coordenadas_longitud: Double
    @NSManaged public var especie: String?
    @NSManaged public var estadoAnimal: String?
    @NSManaged public var facilAcceso: Bool
    @NSManaged public var fecha: NSDate?
    @NSManaged public var indicaciones: String?
    @NSManaged public var nombreDelLugar: String?
    @NSManaged public var numeroEspecimenes: Int16
    @NSManaged public var observaciones: String?
    @NSManaged public var referenciasLugar: String?
    @NSManaged public var tipoSustrato: String?
    @NSManaged public var vistoPorPrimeraVez: String?
    @NSManaged public var fotografiasAgregadas: NSSet?

}

// MARK: Generated accessors for fotografiasAgregadas
extension Varamiento {

    @objc(addFotografiasAgregadasObject:)
    @NSManaged public func addToFotografiasAgregadas(_ value: FotografiaVaramiento)

    @objc(removeFotografiasAgregadasObject:)
    @NSManaged public func removeFromFotografiasAgregadas(_ value: FotografiaVaramiento)

    @objc(addFotografiasAgregadas:)
    @NSManaged public func addToFotografiasAgregadas(_ values: NSSet)

    @objc(removeFotografiasAgregadas:)
    @NSManaged public func removeFromFotografiasAgregadas(_ values: NSSet)

}
