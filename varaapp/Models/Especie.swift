//
//  Especie.swift
//  varaapp
//
//  Created by Missael Hernandez on 02/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit
class Especie {
    
    
//    static var lista = [(TipoAnimal.odontoceto, #imageLiteral(resourceName: "varamiento-1")),
//                            (TipoAnimal.misticeto, #imageLiteral(resourceName: "varamiento-2")),
//                            (TipoAnimal.pinnipedo, #imageLiteral(resourceName: "varamiento-3")),
//                            (TipoAnimal.sirenia, #imageLiteral(resourceName: "varamiento-1")),
//                            (TipoAnimal.mustelido, #imageLiteral(resourceName: "varamiento-1"))]
    
    static let lista = [
        (TipoAnimal.misticeto ,UIImage(named: "misticeto1 MMOTW")),
        (TipoAnimal.misticeto ,UIImage(named: "misticeto2 MMOTW")),
        (TipoAnimal.misticeto ,UIImage(named: "misticeto3 MMOTW")),
        (TipoAnimal.misticeto ,UIImage(named: "misticeto4 MMOTW")),
        (TipoAnimal.misticeto ,UIImage(named: "misticeto5 MMOTW")),
        (TipoAnimal.mustelido ,UIImage(named: "mustelido1 MMOTW")),
        (TipoAnimal.odontoceto ,UIImage(named: "odontoceto1 MMOTW")),
        (TipoAnimal.odontoceto ,UIImage(named: "odontoceto2 MMOTW")),
        (TipoAnimal.odontoceto ,UIImage(named: "odontoceto3 MMOTW")),
        (TipoAnimal.odontoceto ,UIImage(named: "odontoceto4 MMOTW")),
        (TipoAnimal.odontoceto ,UIImage(named: "odontoceto5 MMOTW")),
        (TipoAnimal.odontoceto ,UIImage(named: "odontoceto6 MMOTW")),
        (TipoAnimal.pinnipedo ,UIImage(named: "pinnipedo1 MMOTW")),
        (TipoAnimal.pinnipedo ,UIImage(named: "pinnipedo2 MMOTW")),
        (TipoAnimal.pinnipedo ,UIImage(named: "pinnipedo3 MMOTW")),
        (TipoAnimal.sirenia ,UIImage(named: "sirenia1 MMOTW")),
        (TipoAnimal.sirenia ,UIImage(named: "sirenia2 MMOTW")),
        (TipoAnimal.sirenia ,UIImage(named: "sirenia3 MMOTW")),
        (TipoAnimal.sirenia ,UIImage(named: "sirenia4 MMOTW"))
    ]
    
    private init() {
        
    }
}
