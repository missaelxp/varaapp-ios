//
//  AvisoVarmiento.swift
//  varaapp
//
//  Created by Missael Hernandez on 25/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import CoreData
enum Sustrato {
    case arena
    case grava
    case rocoso
    
    var description:String {
        switch self{
        case .arena:
            return "Arena"
        case .grava:
            return "Grava"
        case .rocoso:
            return "Rocoso"
        }
    }
    
    var serverId : String {
        switch self {
        case .arena:
            return "0"
        case .grava:
            return "1"
        case .rocoso:
            return "2"
        }
    }
    static func fromServerId(id: String) -> Sustrato {
        switch id {
        case "0":
            return Sustrato.arena
        case "1":
            return Sustrato.grava
        case "2":
            return Sustrato.rocoso
        default:
            return Sustrato.arena
        }
    }
}
enum EstadoAnimal {
    case vivo
    case muerto
    case indefinido
    case vivosMuertos
    
    var description : String {
        switch self {
        case .vivo:
            return "Vivo"
        case .muerto:
            return "Muerto"
        case .indefinido:
            return "Indefinido"
        case .vivosMuertos:
            return "Vivos y muertos"
        }
    }
    
    var serverId : String {
        switch self {
        case .vivo:
            return "0"
        case .muerto:
            return "1"
        case .indefinido:
            return "2"
        case .vivosMuertos:
            return "3"
        }
    }
    
    static func fromServerId(id: String) -> EstadoAnimal {
        switch id {
        case "0":
            return EstadoAnimal.vivo
        case "1":
            return EstadoAnimal.muerto
        case "2":
            return EstadoAnimal.indefinido
        case "3":
            return EstadoAnimal.vivosMuertos
        default:
            return EstadoAnimal.vivo
        }
    }
}
enum TipoAnimal {
    case odontoceto
    case misticeto
    case pinnipedo
    case sirenia
    case mustelido
    
    var description : String {
        switch self {
        case .odontoceto:
            return "Odontoceto"
        case .misticeto:
            return "Misticeto"
        case .pinnipedo:
            return "Pinnípedo"
        case .sirenia:
            return "Sirenia"
        case .mustelido:
            return "Mustélido"
        }
    }
    
    var serverId : String {
        switch self {
        case .odontoceto:
            return "0"
        case .misticeto:
            return "1"
        case .pinnipedo:
            return "2"
        case .sirenia:
            return "3"
        case .mustelido:
            return "4"
        }
    }
    
    static func fromServerId(id: String) -> TipoAnimal {
        switch id {
        case "0":
            return TipoAnimal.odontoceto
        case "1":
            return TipoAnimal.misticeto
        case "2":
            return TipoAnimal.pinnipedo
        case "3":
            return TipoAnimal.sirenia
        case "4":
            return TipoAnimal.mustelido
        default:
            return TipoAnimal.odontoceto
        }
    }
}
enum LugarDondeSeVio {
    case playa
    case agua
    
    var description  : String  {
        switch self {
        case .playa:
            return "Playa"
        case .agua:
            return "Agua"
        }
    }
    
    var serverId : String {
        switch self{
        case .playa:
            return "0"
        case .agua:
            return "1"
        }
    }
    
    static func fromServerId(id:String) -> LugarDondeSeVio{
        switch id {
        case "0":
            return LugarDondeSeVio.playa
        case "1":
            return LugarDondeSeVio.agua
        default:
            return LugarDondeSeVio.playa
        }
    }
}

struct AvisoVaramiento{
    var id : Int?                               // - server_key : "id"
    var acantilado : Bool                       // - server_key : "acantilado"
    var facilAcceso : Bool                      // - server_key : "facilAcceso"
    var fecha : Date?                           // - server_key : "fechaDeAvistamiento"
    var numeroEspecimenes : Int                 // - server_key : "cantidadDeAnimales"
    var fotografias : [UIImage]
    var fotografiasURLs : [String]              // - server_key : "fotografias"
    var coordenadas : CLLocationCoordinate2D?   // - server_key : "latitud" // - server_key : "longitud"
    var observaciones : String?                 // - server_key : "observaciones"
    var tipoSustrato : Sustrato                 //- server_key : "sustrato"
    var tipoAnimal : TipoAnimal                 // - server_key : "tipoDeAnimal"
    var estadoAnimal : EstadoAnimal             // - server_key : "condicionDeAnimal"
    var vistoPorPrimeraVez : LugarDondeSeVio    // - server_key : "lugarDondeSeVio"
    
    var indicaciones : String?                  // - server_key : "informacionDeLocalizacion"
    var nombreDelLugar : String?                // - server_key : "informacionDeLocalizacion"
    var referenciasLugar : String?              // - server_key : "informacionDeLocalizacion"
    var enviado :Bool = false
    
    init (id : Int? = nil, acantilado : Bool = true, facilAcceso : Bool = true, fecha : Date? = nil, numeroEspecimenes : Int = 1, fotografias : [UIImage] = [], indicaciones : String? = nil, coordenadas : CLLocationCoordinate2D? = nil, nombreDelLugar : String? = nil, observaciones : String? = nil, referenciasLugar : String? = nil, tipoSustrato : Sustrato = .arena, tipoAnimal : TipoAnimal = .odontoceto, estadoAnimal : EstadoAnimal = .vivo, vistoPorPrimeraVez : LugarDondeSeVio = .playa, enviado : Bool = false, fotografiasURLs : [String] = [String]()){
        self.id = id
        self.acantilado = acantilado
        self.facilAcceso = facilAcceso
        self.fecha = fecha
        self.numeroEspecimenes = numeroEspecimenes
        self.fotografias = fotografias
        self.indicaciones = indicaciones
        self.coordenadas = coordenadas
        self.nombreDelLugar = nombreDelLugar
        self.observaciones = observaciones
        self.referenciasLugar = referenciasLugar
        self.tipoSustrato = tipoSustrato
        self.tipoAnimal = tipoAnimal
        self.estadoAnimal = estadoAnimal
        self.vistoPorPrimeraVez = .playa
        self.enviado = enviado
        self.fotografiasURLs = fotografiasURLs
    }
}



class AvisoPorReportar {
    var id : Int? // - server_key : "id"    
    var acantilado : Bool // - server_key : "acantilado"
    var facilAcceso : Bool// - server_key : "facilAcceso"
    var fecha : Date // - server_key : "fechaDeAvistamiento"
    var numeroEspecimenes : Int // - server_key : "cantidadDeAnimales"
    var fotografias : [UIImage] // - server_key : "fotografias"
    
    var coordenadas : CLLocationCoordinate2D// - server_key : "latitud" // - server_key : "longitud"
    
    var observaciones : String // - server_key : "observaciones"
    
    var tipoSustrato : Sustrato //- server_key : "sustrato"
    var tipoAnimal : TipoAnimal // - server_key : "tipoDeAnimal"
    var estadoAnimal : EstadoAnimal // - server_key : "condicionDeAnimal"
    var vistoPorPrimeraVez : LugarDondeSeVio // - server_key : "lugarDondeSeVio"
    
    var indicaciones : String // - server_key : "informacionDeLocalizacion"
    var nombreDelLugar : String // - server_key : "informacionDeLocalizacion"
    var referenciasLugar : String // - server_key : "informacionDeLocalizacion"
    var enviado :Bool = false
    
    private init() {
        acantilado = true
        facilAcceso = true
        fecha = Date()
        numeroEspecimenes = 1
        fotografias = []
        coordenadas = CLLocationCoordinate2D(latitude: 37.785834000000001, longitude: -122.406417)
        indicaciones = ""
        nombreDelLugar = ""
        observaciones = ""
        referenciasLugar = ""
        tipoSustrato = .arena
        estadoAnimal = .vivo
        tipoAnimal = .odontoceto
        vistoPorPrimeraVez = .playa
        enviado = false
    }
    static let sharedInstance = AvisoPorReportar()
    
    static func clearSharedInstance(){
        self.sharedInstance.acantilado = true
        self.sharedInstance.facilAcceso = true
        self.sharedInstance.fecha = Date()
        self.sharedInstance.numeroEspecimenes = 1
        self.sharedInstance.fotografias = []
        self.sharedInstance.coordenadas = CLLocationCoordinate2D(latitude: 37.785834000000001, longitude: -122.406417)
        self.sharedInstance.indicaciones = ""
        self.sharedInstance.nombreDelLugar = ""
        self.sharedInstance.observaciones = ""
        self.sharedInstance.referenciasLugar = ""
        self.sharedInstance.tipoSustrato = .arena
        self.sharedInstance.estadoAnimal = .vivo
        self.sharedInstance.tipoAnimal = .odontoceto
        self.sharedInstance.vistoPorPrimeraVez = .playa
        self.sharedInstance.enviado = false
    }
    
    static func pausarReporte(){
        let entity = NSEntityDescription.entity(forEntityName: "Varamiento", in: CoreDataManager.context)
        let newVaramiento = Varamiento(entity: entity!, insertInto: CoreDataManager.context)
        
        
        newVaramiento.acantilado = self.sharedInstance.acantilado
        newVaramiento.facilAcceso = self.sharedInstance.facilAcceso
        newVaramiento.fecha = self.sharedInstance.fecha as NSDate
        newVaramiento.numeroEspecimenes = Int16(self.sharedInstance.numeroEspecimenes)
        newVaramiento.coordenadas_latitud = self.sharedInstance.coordenadas.latitude
        newVaramiento.coordenadas_longitud = self.sharedInstance.coordenadas.longitude
        newVaramiento.indicaciones = self.sharedInstance.indicaciones
        newVaramiento.nombreDelLugar = self.sharedInstance.nombreDelLugar
        newVaramiento.observaciones = self.sharedInstance.observaciones
        newVaramiento.referenciasLugar = self.sharedInstance.referenciasLugar
        newVaramiento.tipoSustrato = self.sharedInstance.tipoSustrato.serverId
        newVaramiento.especie = self.sharedInstance.tipoAnimal.serverId
        newVaramiento.vistoPorPrimeraVez = self.sharedInstance.vistoPorPrimeraVez.serverId
        newVaramiento.estadoAnimal = self.sharedInstance.estadoAnimal.serverId
        
        for foto in self.sharedInstance.fotografias {
            let entityFoto = NSEntityDescription.entity(forEntityName: "FotografiaVaramiento", in: CoreDataManager.context)
            let newFoto = FotografiaVaramiento(entity: entityFoto!, insertInto: CoreDataManager.context)
            newFoto.fotografia = UIImageJPEGRepresentation(foto, 0.5)! as NSData
            
            newVaramiento.addToFotografiasAgregadas(newFoto)
        }
        
        
        
        
        do{
            try CoreDataManager.save()
        } catch {
            print("Hubo un error al guardar los datos")
        }
        
        clearSharedInstance()
    }
    
    public static func getFotografiasTotalSizeInMB() -> Double{
        var count = 0
        for foto in self.sharedInstance.fotografias {
            let data = UIImageJPEGRepresentation(foto, 0.5)
            if let imageData = data{
                count += imageData.count
            }
        }
        let result : Double = Double(count) / Double(1024) / Double(1024)
        return result
    }
    
    public static func setFromCoreData(aviso:Varamiento){
        self.clearSharedInstance()
        self.sharedInstance.acantilado = aviso.acantilado
        self.sharedInstance.facilAcceso = aviso.facilAcceso
        if let fecha = aviso.fecha {
            self.sharedInstance.fecha = fecha as Date
        }
        self.sharedInstance.numeroEspecimenes = Int(aviso.numeroEspecimenes)
        self.sharedInstance.coordenadas = CLLocationCoordinate2D(latitude: 37.785834000000001, longitude: -122.406417)
        self.sharedInstance.coordenadas = CLLocationCoordinate2D(latitude: aviso.coordenadas_latitud, longitude: aviso.coordenadas_longitud)
        self.sharedInstance.indicaciones = aviso.indicaciones ?? ""
        self.sharedInstance.nombreDelLugar = aviso.nombreDelLugar ?? ""
        self.sharedInstance.observaciones = aviso.observaciones ?? ""
        self.sharedInstance.referenciasLugar = aviso.referenciasLugar ?? ""
        self.sharedInstance.tipoSustrato = Sustrato.fromServerId(id: aviso.tipoSustrato ?? "")
        self.sharedInstance.estadoAnimal = EstadoAnimal.fromServerId(id: aviso.estadoAnimal ?? "")
        self.sharedInstance.tipoAnimal = TipoAnimal.fromServerId(id: aviso.especie ?? "")
        self.sharedInstance.vistoPorPrimeraVez = LugarDondeSeVio.fromServerId(id: aviso.vistoPorPrimeraVez ?? "")
        self.sharedInstance.enviado = false
        self.sharedInstance.fotografias = []
        if let fotosAgregadas = aviso.fotografiasAgregadas {
            for foto in fotosAgregadas.allObjects {
                let fotografia = foto as! FotografiaVaramiento
                let imgData = fotografia.fotografia! as Data
                let imageFromData = UIImage(data: imgData)
                if let imageDone = imageFromData {
                    self.sharedInstance.fotografias.append(imageDone)
                }
            }
        }
        
        CoreDataManager.context.delete(aviso)
        do{
            try CoreDataManager.save()
        } catch {
            
        }
    }
}

