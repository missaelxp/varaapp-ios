//
//  AvisoVaramientoCellModel.swift
//  varaapp
//
//  Created by Missael Hernandez on 25/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit

class AvisoVaramientoCellModel {
    var image : UIImage
    var fecha : String
    var numEspecimenes : Int
    
    init (image : UIImage, fecha : String, numEspecimenes : Int){
        self.image = image
        self.fecha = fecha
        self.numEspecimenes = numEspecimenes
    }
}
