//
//  PreguntasDicotonicas.swift
//  varaapp
//
//  Created by Missael Hernandez on 30/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation

class PreguntaDicotomica {
    var pregunta : String?
    var respuesta1 : String?
    var respuesta2 : String?
    
    var esFinal : Bool
    
    var nuevaPregunta1 : PreguntaDicotomica?
    var nuevaPregunta2 : PreguntaDicotomica?
    
    var resultado : TipoAnimal?
    
    init(pregunta: String?, respuesta1: String?, respuesta2: String?, esFinal: Bool, nuevaPregunta1: PreguntaDicotomica?, nuevaPregunta2: PreguntaDicotomica?, resultado: TipoAnimal?) {
        self.pregunta = pregunta
        self.respuesta1 = respuesta1
        self.respuesta2 = respuesta2
        self.esFinal = esFinal
        self.nuevaPregunta1 = nuevaPregunta1
        self.nuevaPregunta2 = nuevaPregunta2
        self.resultado = resultado
    }
    
    init() {
        pregunta = nil
        respuesta1 = nil
        respuesta2 = nil
        
        esFinal = false
        
        nuevaPregunta1 = nil
        nuevaPregunta2 = nil
        
        resultado = nil
    }
}



