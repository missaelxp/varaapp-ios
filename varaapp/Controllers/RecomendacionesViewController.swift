//
//  RecomendacionesViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 28/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class RecomendacionesViewController: UIPageViewController {
    
    lazy var orderedViewControllers : [UIViewController] = {
        return [self.newVc(viewController: "vcRecomendacion1"),
                self.newVc(viewController: "vcRecomendacion2"),
                self.newVc(viewController: "vcRecomendacion3"),
                self.newVc(viewController: "vcRecomendacion4"),
                self.newVc(viewController: "vcRecomendacion5"),
                self.newVc(viewController: "vcRecomendacion6")
                ]
    }()
    
    var pageControl = UIPageControl()
    var btnBack = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        if let firstViewController = orderedViewControllers.first{
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        configurePageControl()
        configureBackButton()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //pageControl.customPageControl(dotFillColor: UIColor.varaapp.blue, dotBorderColor: UIColor.varaapp.blue, dotBorderWidth: 1)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }
    }

    func newVc(viewController : String) -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    func configurePageControl (){
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
//        pageControl.tintColor = UIColor.varaapp.blue
//        pageControl.pageIndicatorTintColor = UIColor.varaapp.blue
//        pageControl.currentPageIndicatorTintColor = UIColor.varaapp.yellow
        
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: UIColor.varaapp.blue)
        self.pageControl.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        self.pageControl.currentPageIndicatorTintColor = UIColor.varaapp.blue
        
        pageControl.isUserInteractionEnabled = false
        
        self.view.addSubview(pageControl)
        
    }
    func configureBackButton(){
        btnBack = UIButton(frame: CGRect(x: view.frame.minX, y: view.frame.minY + 20, width: 50, height: 50))
        
        let origImage = #imageLiteral(resourceName: "exit")
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(tintedImage, for: .normal)
        btnBack.setTitleColor(UIColor.varaapp.blue, for: .normal)
        btnBack.tintColor = UIColor.varaapp.blue
        
        btnBack.addTarget(self, action: #selector(self.btnBackClic(_:)), for: .touchUpInside)
        
        self.view.addSubview(btnBack)
    }
    @objc func btnBackClic(_ sender:UIButton!)
    {
        self.dismiss(animated: true, completion: nil)
    }

}

extension RecomendacionesViewController :  UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {return nil}
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
            //descomentar lo siguiente oara no hacerlo infinito
            //return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex ]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {return nil}
        let nextIndex = viewControllerIndex + 1
        
        guard  orderedViewControllers.count != nextIndex else {
            return orderedViewControllers.first
            //descomentar lo siguiente oara no hacerlo infinito
            //return nil
        }
        
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex ]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let pageContentViewController = pageViewController.viewControllers![0]
        
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
}
