//
//  HomeViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 19/05/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import Reachability
//import BRYXBanner
import SwiftKeychainWrapper

class HomeViewController: UIViewController {
    @IBOutlet weak var btnReportes: UIButton!
    @IBOutlet weak var btnRecomendaciones: UIButton!
    @IBOutlet weak var btnPerfil: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    //var banner : Banner = Banner()
    var isBannerShow : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ReachabilityManager.shared.addListener(listener: self)
        self.handleNetworkStatus(status: ReachabilityManager.shared.reachability.connection)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !AppStatus.shared.seHaIniciadoSesion {
            verificarUsuario()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnReportes.set(image: #imageLiteral(resourceName: "menu-varamientos"), title: "Reportes de varamientos", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .normal)
        btnRecomendaciones.set(image: #imageLiteral(resourceName: "menu-recomendaciones"), title: "Recomendaciones", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .normal)
        btnPerfil.set(image: #imageLiteral(resourceName: "menu-perfil"), title: "Perfil", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .normal)
        
        
        btnReportes.set(image: #imageLiteral(resourceName: "menu-varamientos-pressed"), title: "Reportes de varamientos", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .highlighted)
        btnRecomendaciones.set(image: #imageLiteral(resourceName: "menu-recomendaciones-pressed"), title: "Recomendaciones", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .highlighted)
        btnPerfil.set(image: #imageLiteral(resourceName: "menu-perfil-pressed"), title: "Perfil", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .highlighted)
        
    }

    
    @IBAction func returnToHome(segue:UIStoryboardSegue) { }
    
    func handleNetworkStatus(status: Reachability.Connection){
//        switch status{
//        case .cellular, .wifi:
////            if self.isBannerShow{
////                self.banner.dismiss()
////                self.isBannerShow = false
////            }
//            
//            self.btnPerfil.isEnabled = true
//            
//        case .none:
//            //print("Sin wifi")
////            if self.isBannerShow == false{
////                self.banner = BannerFactory.getBanner(.noInternetInterno)
////                self.banner.show()
////                self.isBannerShow = true
////                self.btnPerfil.isEnabled = false
////            }
//        }
    }
    #warning ("Cachar los errores en el inicio de sesion")
    func verificarUsuario(){
        //preguntamos si el usuario ha decidido mantener la sesion abierta
        if (Default.mantenerSesionIniciada == false){
            solicitarInicioSesion()
        } else {
            // si la sesion sigue abierta buscamos el token
            if let token = KeychainWrapper.standard.string(forKey: Config.KeychainKeys.token) {
                showToast(message: "Iniciando sesión")
                Services.verificarToken(token, onComplete: {
                    AppStatus.shared.usageStatus = .logueado
                    AppStatus.shared.seHaIniciadoSesion = true
                    self.mainView.layer.opacity = 1
                    Default.ultimoInicioSesion = Date()
                    
                }) { (error) in
                    switch error {
                    case .badRequest(_), .existingUser, .notFound, .serverSide, .unauthorized, .timeOut:
                        self.solicitarInicioSesion()
                    case .onlyWifiAllowed:
                        let alert = UIAlertController(title: "Solo Wi-Fi permitido", message: "Solo se permite el uso de red Wi-Fi. Cambia la configuración en la sección de Perfil.", preferredStyle: .alert)
                        alert.addAction(title: "OK", style: .default, handler: { (_) in
                            self.solicitarInicioSesion()
                        })
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            } else {
                self.solicitarInicioSesion()
            }
        }
    
        
    }
    
    func solicitarInicioSesion(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "AuthScreens", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            controller.loginDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
}

extension HomeViewController : NetworkStatusListener {
    func networkStatusDidChange(status: Reachability.Connection) {
        handleNetworkStatus(status:status)
    }
}
extension HomeViewController : LoginDelegate {
    func showMainLayer(){
        self.mainView.layer.opacity = 1
    }
    func onLogin() {
        showMainLayer()
    }
    
}
