//
//  TerminosYCondicionesViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 11/27/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class TerminosYCondicionesViewController: UIViewController {

    @IBOutlet weak var btnExit: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Términos y condiciones"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.varaapp.blue
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @IBAction func onExit(_ sender: Any) {
        self.dismiss(animated: true)
    }
    

}
