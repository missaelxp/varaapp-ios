//
//  OfflineHomeViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/16/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class OfflineHomeViewController: UIViewController {
    @IBOutlet weak var btnAgregarReporte: UIButton!
    @IBOutlet weak var btnRecomendaciones: UIButton!
    @IBOutlet weak var btnIniciarSesion: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnAgregarReporte.set(image: #imageLiteral(resourceName: "menu-varamientos"), title: "Guardar varamiento en el dispositivo", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .normal)
        btnRecomendaciones.set(image: #imageLiteral(resourceName: "menu-recomendaciones"), title: "Recomendaciones", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .normal)
        btnIniciarSesion.set(image: #imageLiteral(resourceName: "menu-perfil"), title: "Iniciar sesion", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .normal)
        
        btnAgregarReporte.set(image: #imageLiteral(resourceName: "menu-varamientos-pressed"), title: "Guardar varamiento en el dispositivo", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .highlighted)
        btnRecomendaciones.set(image: #imageLiteral(resourceName: "menu-recomendaciones-pressed"), title: "Recomendaciones", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .highlighted)
        btnIniciarSesion.set(image: #imageLiteral(resourceName: "menu-perfil-pressed"), title: "Iniciar sesion", titlePosition: .bottom, additionalSpacing: CGFloat(0), state: .highlighted)
    }
    @IBAction func onIniciarSesionClic(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func returnToOfflineHome(segue:UIStoryboardSegue) { }
    
}
