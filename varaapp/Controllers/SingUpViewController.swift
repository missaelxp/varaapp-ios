//
//  SingUpViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 17/07/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SingUpViewController: UIViewController {
    @IBOutlet weak var btnRegistrar: UIButton!
    @IBOutlet var formStackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellidoPaterno: UITextField!
    @IBOutlet weak var txtApellidoMaterno: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtInstitucion: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    
    @IBOutlet weak var lbErrorNombre: UILabel!
    @IBOutlet weak var lbErrorAPaterno: UILabel!
    @IBOutlet weak var lbErrorAMaterno: UILabel!
    @IBOutlet weak var lbErrorTelefono: UILabel!
    @IBOutlet weak var lbErrorInstitucion: UILabel!
    @IBOutlet weak var lbErrorCorreo: UILabel!
    @IBOutlet weak var lbErrorPassword: UILabel!
    @IBOutlet weak var lbErrorPasswordRepeat: UILabel!
    
    @IBOutlet weak var btnTerminos: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRegistrar.layer.cornerRadius = btnRegistrar.frame.height / 2
        btnRegistrar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        btnRegistrar.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnRegistrar.layer.shadowOpacity = 1.0
        btnRegistrar.layer.shadowRadius = btnRegistrar.frame.height / 2
        btnRegistrar.layer.masksToBounds = true
        
        btnRegistrar.titleLabel?.layer.shadowRadius = 2
        btnRegistrar.titleLabel?.layer.shadowColor = UIColor.black.cgColor
        btnRegistrar.titleLabel?.layer.shadowOffset = CGSize(width: 0, height: 0)
        btnRegistrar.titleLabel?.layer.shadowOpacity = 0.8
        btnRegistrar.titleLabel?.layer.masksToBounds = true
        
        self.scrollView.addSubview(self.formStackView)
       
        
        self.formStackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.formStackView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor).isActive = true
        self.formStackView.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor).isActive = true
        self.formStackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        self.formStackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        
        self.formStackView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        
        formStackView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 20)
        formStackView.isLayoutMarginsRelativeArrangement = true
        
        setupErrosLabels()
        
        btnTerminos.contentEdgeInsets = UIEdgeInsets(top: 2, left: 12, bottom: 10, right: 12)
        btnTerminos.tintColor = .black
        btnTerminos.titleLabel?.lineBreakMode = .byWordWrapping
        btnTerminos.titleLabel?.textAlignment = .justified
        btnTerminos.titleLabel?.numberOfLines = 0
        btnTerminos.setTitleColor(UIColor.white, for: .normal)
        btnTerminos.titleLabel?.font =  .systemFont(ofSize: 12)
    }

    func setupErrosLabels() {
        lbErrorNombre.layer.cornerRadius = 5
        lbErrorNombre.layer.masksToBounds = true
        lbErrorAPaterno.layer.cornerRadius = 5
        lbErrorAPaterno.layer.masksToBounds = true
        lbErrorAMaterno.layer.cornerRadius = 5
        lbErrorAMaterno.layer.masksToBounds = true
        lbErrorTelefono.layer.cornerRadius = 5
        lbErrorTelefono.layer.masksToBounds = true
        lbErrorInstitucion.layer.cornerRadius = 5
        lbErrorInstitucion.layer.masksToBounds = true
        lbErrorCorreo.layer.cornerRadius = 5
        lbErrorCorreo.layer.masksToBounds = true
        lbErrorPassword.layer.cornerRadius = 5
        lbErrorPassword.layer.masksToBounds = true
        lbErrorPasswordRepeat.layer.cornerRadius = 5
        lbErrorPasswordRepeat.layer.masksToBounds = true
    }
    
    func setAllErrorsLabelsOn(){
        lbErrorNombre.isHidden = false
        lbErrorAPaterno.isHidden = false
        lbErrorAMaterno.isHidden = false
        lbErrorTelefono.isHidden = false
        lbErrorInstitucion.isHidden = false
        lbErrorCorreo.isHidden = false
        lbErrorPassword.isHidden = false
        lbErrorPasswordRepeat.isHidden = false
    }
    func setAllErrorsLabelsOff(){
        lbErrorNombre.isHidden = true
        lbErrorAPaterno.isHidden = true
        lbErrorAMaterno.isHidden = true
        lbErrorTelefono.isHidden = true
        lbErrorInstitucion.isHidden = true
        lbErrorCorreo.isHidden = true
        lbErrorPassword.isHidden = true
        lbErrorPasswordRepeat.isHidden = true
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validarCampos() -> Bool{
        if txtNombre.text == "" || !(txtNombre.text?.isValidUserField())!{
            self.lbErrorNombre.isHidden = false
            return false
        }
        if txtTelefono.text == "" || !(txtTelefono.text?.isValidPhone())! {
            self.lbErrorTelefono.isHidden = false
            txtTelefono.becomeFirstResponder()
            return false
        }
        if txtCorreo.text == "" || !(txtCorreo.text?.isValidEmail())! {
            self.lbErrorCorreo.isHidden = false
            txtCorreo.becomeFirstResponder()
            return false
        }
        
        if txtPassword.text == "" || !(txtPassword.text?.isValidPassword())! {
            self.lbErrorPassword.isHidden = false
            self.txtPassword.becomeFirstResponder()
            return false
        }
        
        if txtPassword.text != txtConfirmPassword.text {
            self.lbErrorPasswordRepeat.isHidden = false
            self.txtPassword.becomeFirstResponder()
            return false
        }
        
        
        // campos opcionales
        if txtApellidoPaterno.text != "" && !(txtApellidoPaterno.text?.isValidUserField())!{
            self.lbErrorAPaterno.isHidden = false
            return false
        }
        if txtApellidoMaterno.text != "" && !(txtApellidoMaterno.text?.isValidUserField())!{
            self.lbErrorAMaterno.isHidden = false
            return false
        }
        if txtInstitucion.text != "" && !(txtInstitucion.text?.isValidUserField())!{
            self.lbErrorInstitucion.isHidden = false
            return false
        }
        
        
        
        return true
    }
    
    
    @IBAction func onNombreChange(_ sender: Any) {
        if txtNombre.text == "" || !(txtNombre.text?.isValidUserField())!{
            self.lbErrorNombre.isHidden = false
        } else {
            self.lbErrorNombre.isHidden = true
        }
    }
    @IBAction func onAPaternoChange(_ sender: Any) {
        if txtApellidoPaterno.text != "" && !(txtApellidoPaterno.text?.isValidUserField())!{
            self.lbErrorAPaterno.isHidden = false
        } else {
            self.lbErrorAPaterno.isHidden = true
        }
    }
    @IBAction func onAMaternoChange(_ sender: Any) {
        if txtApellidoMaterno.text != "" && !(txtApellidoMaterno.text?.isValidUserField())!{
            self.lbErrorAMaterno.isHidden = false
        } else {
            self.lbErrorAMaterno.isHidden = true
        }
    }
    @IBAction func onTelefonoChange(_ sender: Any) {
        if txtTelefono.text == "" || !(txtTelefono.text?.isValidPhone())! {
            self.lbErrorTelefono.isHidden = false
        } else {
            self.lbErrorTelefono.isHidden = true
        }
    }
    @IBAction func onInstitucionChange(_ sender: Any) {
        if txtInstitucion.text != "" && !(txtInstitucion.text?.isValidUserField())!{
            self.lbErrorInstitucion.isHidden = false
        } else {
            self.lbErrorInstitucion.isHidden = true
        }
    }
    @IBAction func onCorreoChange(_ sender: Any) {
        if txtCorreo.text == "" || !(txtCorreo.text?.isValidEmail())! {
            self.lbErrorCorreo.isHidden = false
        } else {
            self.lbErrorCorreo.isHidden = true
        }
    }
    @IBAction func onPasswordChange(_ sender: Any) {
        if txtPassword.text == "" || !(txtPassword.text?.isValidPassword())! {
            self.lbErrorPassword.isHidden = false
        } else {
            self.lbErrorPassword.isHidden = true
        }
    }
    @IBAction func onPassword_repeatChange(_ sender: Any) {
        if txtPassword.text != txtConfirmPassword.text {
            self.lbErrorPasswordRepeat.isHidden = false
        } else {
            self.lbErrorPasswordRepeat.isHidden = true
        }
    }
    
    @IBAction func onRegistrarClic(_ sender: Any) {
        if validarCampos() == false {return}
        
        self.btnRegistrar.loadingIndicator(true)
        Services.registrarUsuario(nombre: txtNombre.text!, apellidoPaterno: txtApellidoPaterno.text!, apellidoMaterno: txtApellidoMaterno.text!, correo: txtCorreo.text!, password: txtPassword.text!, passwordRepeat: txtConfirmPassword.text!, telefono: txtTelefono.text!, institucion: txtInstitucion.text!, onComplete: { (token, _) in
            
            AppStatus.shared.usageStatus = .logueado
            KeychainWrapper.standard.set(token, forKey: Config.KeychainKeys.token)
            DispatchQueue.main.async {
                self.btnRegistrar.loadingIndicator(false)
                let alert = UIAlertController(title: "Cuenta creada", message: "Tu cuenta se ha creado, te redireccionaremos a la pantalla de inicio", preferredStyle: .alert)
                alert.addAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    DispatchQueue.main.async {
                        let loginVC = self.presentingViewController as! LoginViewController
                        loginVC.loginDelegate?.onLogin()
                        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                    }
                })
                self.present(alert, animated: true, completion: nil)
            }
        }) { (error) in
            self.btnRegistrar.loadingIndicator(false)
            switch(error) {
            case .badRequest:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Verifica los campos", message: "Un campo no cumple con los requerimientos. Verifica los mensajes de error y reintenta.", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .existingUser:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "El correo está en uso", message: "El correo proporcionado ya es usado por otro informante", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .notFound,
                 .serverSide,
                 .unauthorized,
                 .timeOut:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Hubo un error en la conexión con el servidor", message: "Verifica tu conexión y vuelve a intentar", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .onlyWifiAllowed:
                DispatchQueue.main.async {
                    self.showToast(message: "Solo se permite el uso de red Wi-Fi. Cambia la configuración en la sección de Perfil.")
                }
            }
        }
    }
    
}
