//
//  ReportesPausadosCell.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/18/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class ReportesPausadosCell: UITableViewCell {
    
    var aviso : Varamiento?
    
    @IBOutlet weak var imgReporte: UIImageView!
    @IBOutlet weak var lbFecha: UILabel!
    @IBOutlet weak var lbNumeroEspecimenes: UILabel!
    

    func setAviso(aviso: Varamiento){
        if let date = aviso.fecha! as Date?{
            lbFecha.text = date.getPrintFecha(format: "dd/MM/yyyy")
        }
        lbNumeroEspecimenes.text = "\(aviso.numeroEspecimenes)"
        
        if let fotosAgregadas = aviso.fotografiasAgregadas {
            if fotosAgregadas.allObjects.count > 0 {
                let fotografia = fotosAgregadas.allObjects[0] as! FotografiaVaramiento
                let imgData = fotografia.fotografia! as Data
                let imageFromData = UIImage(data: imgData)
                imgReporte.image = imageFromData
            } else {
                imgReporte.image = #imageLiteral(resourceName: "FOTO_reporte_sin_fotos")
            }
            
        } else {
            imgReporte.image = #imageLiteral(resourceName: "FOTO_reporte_sin_fotos")
        }
    }

}
