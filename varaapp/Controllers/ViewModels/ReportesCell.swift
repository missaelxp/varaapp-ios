//
//  ReportesCell.swift
//  varaapp
//
//  Created by Missael Hernandez on 25/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class ReportesCell: UITableViewCell {

    @IBOutlet weak var imgReporte: UIImageView!
    @IBOutlet weak var lbFecha: UILabel!
    @IBOutlet weak var lbNumEspecimenes: UILabel!
    
    
    func setAviso(aviso: AvisoVaramiento){
        if aviso.fotografias.count > 0{
            imgReporte.image = aviso.fotografias[0]
        }
        if aviso.fotografiasURLs.count > 0 {
            let imageLink = "\(ApiEndpoint.baseURL)\(aviso.fotografiasURLs[0])"
            imgReporte.downloadedFrom(url: imageLink)
        } else {
            imgReporte.image = #imageLiteral(resourceName: "FOTO_reporte_sin_fotos")
        }
        
        lbFecha.text = aviso.fecha?.getPrintFecha(format: "dd/MM/yyyy")
        lbNumEspecimenes.text = "\(aviso.numeroEspecimenes)"
    }
    
    
    

}
