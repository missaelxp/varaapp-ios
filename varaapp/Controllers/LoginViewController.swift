//
//  ViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 18/05/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import Reachability
//import BRYXBanner
import IQKeyboardManagerSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var btnEntrar: UIButton!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRegistrarUsuario: UIButton!
    var loginDelegate : LoginDelegate?
    
//    var banner : Banner = Banner()
    var isBannerShow : Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnEntrar.layer.cornerRadius = btnEntrar.frame.height / 2
        btnEntrar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        btnEntrar.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnEntrar.layer.shadowOpacity = 1.0
        btnEntrar.layer.shadowRadius = btnEntrar.frame.height / 2
        btnEntrar.layer.masksToBounds = false
        btnEntrar.backgroundColor = UIColor.varaapp.blue
        txtCorreo.text = Default.ultimoUsuario
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ReachabilityManager.shared.addListener(listener: self)
        self.handleNetworkStatus(status: ReachabilityManager.shared.reachability.connection)
        txtCorreo.text = Default.ultimoUsuario
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    
    @IBAction func onBtnOfflineClic(_ sender: Any) {
        AppStatus.shared.usageStatus = .fueraDeLinea
        self.performSegue(withIdentifier: "offlineHomeSegue", sender: nil)
    }
    
    @IBAction func btnEntrarClicked(_ sender: Any) {
        self.btnEntrar.loadingIndicator(true)
        Services.iniciarSesion(correo: self.txtCorreo.text ?? "", password: self.txtPassword.text ?? "", onComplete: { (token, expirationDate) in
            KeychainWrapper.standard.set(token, forKey: Config.KeychainKeys.token)
            AppStatus.shared.usageStatus = .logueado
            AppStatus.shared.seHaIniciadoSesion = true
            Default.ultimoUsuario = self.txtCorreo.text!
            Default.ultimoInicioSesion = Date()
            
            DispatchQueue.main.async {
                self.btnEntrar.loadingIndicator(false)
                if self.loginDelegate != nil {
                    self.loginDelegate?.onLogin()
                }
                self.dismiss(animated: true, completion: nil)
            }
        }) { (error) in
            self.btnEntrar.loadingIndicator(false)
            switch error {
            case .badRequest(_):
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Correo electrónico  o contraseña incorrecta.", message: "Intenta de nuevo", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .existingUser, .notFound, .serverSide:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error de conexión", message: "Verifica tu conexión a internet", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .unauthorized:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Correo electrónico  o contraseña incorrecta", message: "Intenta de nuevo", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .timeOut:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error de conexión", message: "Verifica tu conexión a internet", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .onlyWifiAllowed:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Estás sobre datos celulares", message: "Solo se permite el uso de red Wi-Fi. Cambia la configuración en la sección de Perfil.", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    
    
    func handleNetworkStatus(status : Reachability.Connection){
        
    }
    

}

extension LoginViewController : NetworkStatusListener {
    func networkStatusDidChange(status: Reachability.Connection) {
        handleNetworkStatus(status: status)
    }
}



