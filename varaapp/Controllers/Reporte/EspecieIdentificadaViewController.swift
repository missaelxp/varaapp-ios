//
//  EspecieIdentificadaViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 03/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class EspecieIdentificadaViewController: UIViewController {
    @IBOutlet weak var lbEspecie: UILabel!
    @IBOutlet weak var btnVerAviso: UIButton!
    var seleccionarEspecieDelegate : SeleccionarEspecieDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbEspecie.text = AvisoPorReportar.sharedInstance.tipoAnimal.description
        btnVerAviso.layer.cornerRadius = btnVerAviso.frame.height / 2
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onContinuarClic(_ sender: Any) {
        self.seleccionarEspecieDelegate.onEspecieDefinida(especie: AvisoPorReportar.sharedInstance.tipoAnimal)
        self.dismiss(animated: true, completion: nil)
    }
    

}
