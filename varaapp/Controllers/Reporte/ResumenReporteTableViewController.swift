//
//  ResumenReporteTableViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 02/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import MapKit
import SwiftKeychainWrapper


class ResumenReporteTableViewController: UITableViewController {
    
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var lbLatitud: UILabel!
    @IBOutlet weak var lbLongitud: UILabel!
    @IBOutlet weak var lbEspecieIdentificada: UILabel!
    @IBOutlet weak var txtNumeroEspecimenes: UITextField!
    
    @IBOutlet weak var txtTipoSustrato: UITextField!
    @IBOutlet weak var imgCabecera: UIImageView!
    @IBOutlet weak var lbNumeroFotos: UILabel!
    @IBOutlet weak var txtFirstSeen: UITextField!
    @IBOutlet weak var txtIndicaciones: UITextView!
    @IBOutlet weak var txtReferencias: UITextView!
    @IBOutlet weak var txtObservaciones: UITextView!
    @IBOutlet weak var txtNombreLugar: UITextView!
    
    @IBOutlet weak var lbEsDeFacilAcceso: UILabel!
    @IBOutlet weak var switchEsDeFacilAcceso: UISwitch!
    
    @IBOutlet weak var lbEsUnAcantilado: UILabel!
    @IBOutlet weak var swiftchEsUnAcantilado: UISwitch!
    
    @IBOutlet weak var btnEnviarReporte: UIButton!
    @IBOutlet weak var resumenTableView: UITableView!
    
    @IBOutlet weak var txtEstadoEspecimen: UITextField!
    
    
    //celdas para detailDiscluser
    @IBOutlet weak var fechaCell: UITableViewCell!
    @IBOutlet weak var latitudCell: UITableViewCell!
    @IBOutlet weak var longitudCell: UITableViewCell!
    @IBOutlet weak var estadoEspecimenCell: UITableViewCell!
    @IBOutlet weak var especieCell: UITableViewCell!
    @IBOutlet weak var tipoSustratoCell: UITableViewCell!
    @IBOutlet weak var firstSeenCell: UITableViewCell!
    @IBOutlet weak var numeroEspecimenesCell: UITableViewCell!
    
    //Formateador de fechas
    let dateFormatter : DateFormatter = {
        let temp = DateFormatter()
        temp.dateFormat = "dd/MM/yyyy"
        return temp
    }()
    
    
    let datePicker = UIDatePicker()
    
    //pickers
    let pickerEstadoEspecimen = UIPickerView()
    let pickerSustrato = UIPickerView()
    let pickerFirstSeen = UIPickerView()
    
    //data forThePickers
    var dataEstadoEspecimen : [String] = []
    let dataMultipleEstadoEspecimen = ["Vivo", "Muerto", "Indefinido", "Vivos y muertos"]
    let dataSingleEstadoEspecimen = ["Vivo", "Muerto", "Indefinido"]
    let dataSustrato = ["Arena","Grava" , "Rocoso"]
    let dataFirstSeen = ["Playa", "Agua"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Verificamos si el usuario se ha logueado o si está como anonimo
        switch AppStatus.shared.usageStatus {
        case .fueraDeLinea:
            self.btnEnviarReporte.setTitle("Guardar reporte", for: .normal)
        case .logueado:
            self.btnEnviarReporte.setTitle("Enviar reporte", for: .normal)
        }
        
        loadLabelsFromAviso()
        reloadPickerEstadoEspecimen()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.varaapp.blue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        btnEnviarReporte.layer.cornerRadius = btnEnviarReporte.frame.height / 2
        resumenTableView.separatorColor = UIColor.groupTableViewBackground;
        
        //Definir los estilos de celda
        latitudCell.accessoryType = .disclosureIndicator
        longitudCell.accessoryType = .disclosureIndicator
        //estadoEspecimenCell.accessoryType = .disclosureIndicator
        especieCell.accessoryType = .disclosureIndicator
        //tipoSustratoCell.accessoryType = .disclosureIndicator
        //firstSeenCell.accessoryType = .disclosureIndicator
        //numeroEspecimenesCell.accessoryType = .disclosureIndicator
        
        createDatePicker()
        createPickers()
        
        if  let window = (UIApplication.shared.delegate?.window)! as UIWindow? {
            window.backgroundColor = UIColor.varaapp.blue
        }
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "identificarEspecieResumenSegue"{
            let navController = segue.destination as! UINavigationController
            let vc = navController.topViewController as! SeleccionarEspecieViewController
            vc.especieDelegate = self as SeleccionarEspecieDelegate
        } else if segue.identifier == "resumenFotografiasSegue" {
            let navController = segue.destination as! UINavigationController
            let vc = navController.topViewController as! FotografiasViewController
            vc.fotografiasDelegate = self
        }
    }

    
    func createDatePicker(){
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneDatePicker))
        toolbar.setItems([doneButton], animated: true)
        txtFecha.inputAccessoryView = toolbar
        txtFecha.inputView = datePicker
    }
    func createPickers(){
        pickerEstadoEspecimen.dataSource = self
        pickerSustrato.dataSource = self
        pickerFirstSeen.dataSource = self
        pickerEstadoEspecimen.delegate = self
        pickerSustrato.delegate = self
        pickerFirstSeen.delegate = self
        
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePicker))
        toolbar.setItems([doneButton], animated: true)
        
        txtEstadoEspecimen.inputAccessoryView = toolbar
        txtTipoSustrato.inputAccessoryView = toolbar
        txtFirstSeen.inputAccessoryView = toolbar
        
        txtEstadoEspecimen.inputView = pickerEstadoEspecimen
        txtTipoSustrato.inputView = pickerSustrato
        txtFirstSeen.inputView = pickerFirstSeen
        
        
    }
    func reloadPickerEstadoEspecimen(){
        if(AvisoPorReportar.sharedInstance.numeroEspecimenes > 1){
            self.dataEstadoEspecimen = dataMultipleEstadoEspecimen
        } else {
            self.dataEstadoEspecimen = dataSingleEstadoEspecimen
        }
        
        pickerEstadoEspecimen.reloadAllComponents()
    }
    
    @objc func doneDatePicker(){
        if Date() < datePicker.date {
            self.view.endEditing(true)
            let alert = UIAlertController(title: "Fecha incorrecta", message: "Selecciona una fecha menor o igual a la actual", preferredStyle: .alert)
            alert.addAction(title: "OK", style: .default, handler: nil)
            self.present(alert, animated: true, completion: nil)
        } else {
            AvisoPorReportar.sharedInstance.fecha = datePicker.date
            loadLabelsFromAviso()
            self.view.endEditing(true)
        }
        
    }
    @objc func donePicker(){
        self.view.endEditing(true)
    }
    

    func loadLabelsFromAviso(){
        
        
        imgCabecera.image = AvisoPorReportar.sharedInstance.fotografias[0]
        lbNumeroFotos.text = "Fotografías: \(AvisoPorReportar.sharedInstance.fotografias.count)"
        txtFecha.text = dateFormatter.string(from: AvisoPorReportar.sharedInstance.fecha)
        lbLatitud.text = "\(AvisoPorReportar.sharedInstance.coordenadas.latitude.representacionEnGrados(esLatitud: true))"
        lbLongitud.text = "\(AvisoPorReportar.sharedInstance.coordenadas.longitude.representacionEnGrados(esLatitud: false))"
        
        var estadoAnimal = ""
        switch AvisoPorReportar.sharedInstance.estadoAnimal {
        case .indefinido:
            estadoAnimal = "Indefinido"
        case .muerto:
            estadoAnimal = "Muerto"
        case .vivo:
            estadoAnimal = "Vivo"
        case .vivosMuertos:
            estadoAnimal = "Vivos y muertos"
        }
        
        txtEstadoEspecimen.text = estadoAnimal
        
        lbEspecieIdentificada.text = AvisoPorReportar.sharedInstance.tipoAnimal.description
        txtNumeroEspecimenes.text = "\(AvisoPorReportar.sharedInstance.numeroEspecimenes)"
        txtTipoSustrato.text = AvisoPorReportar.sharedInstance.tipoSustrato.description
        txtFirstSeen.text = AvisoPorReportar.sharedInstance.vistoPorPrimeraVez.description
        
        lbEsDeFacilAcceso.text = (AvisoPorReportar.sharedInstance.facilAcceso) ? "Si" : "No"
        switchEsDeFacilAcceso.isOn = AvisoPorReportar.sharedInstance.facilAcceso
        lbEsUnAcantilado.text = (AvisoPorReportar.sharedInstance.acantilado) ? "Si" : "No"
        swiftchEsUnAcantilado.isOn = AvisoPorReportar.sharedInstance.acantilado
        
        txtNombreLugar.text = AvisoPorReportar.sharedInstance.nombreDelLugar
        txtIndicaciones.text = AvisoPorReportar.sharedInstance.indicaciones
        txtReferencias.text = AvisoPorReportar.sharedInstance.referenciasLugar
        txtObservaciones.text = AvisoPorReportar.sharedInstance.observaciones
        
    }
    @IBAction func esDeFacilAccesoChange(_ sender: UISwitch) {
        AvisoPorReportar.sharedInstance.facilAcceso = sender.isOn
        loadLabelsFromAviso()
    }
    
    @IBAction func esAcantiladoChange(_ sender: UISwitch) {
        AvisoPorReportar.sharedInstance.acantilado = sender.isOn
        loadLabelsFromAviso()
    }
    
    @IBAction func onHomeClic(_ sender: Any) {
        var segueToPerform : String = ""
        switch AppStatus.shared.usageStatus {
        case .fueraDeLinea:
            segueToPerform = "resumenReturnOfflineHomeSegue"
        case .logueado:
            segueToPerform = "resumenReturnHomeSegue"
        }
        
        let alert = UIAlertController(title: "Estas a punto de salir", message: "¿Deseas pausar el reporte?", preferredStyle: .alert)
        alert.addAction(title: "Continuar reporte", style: .default, handler: nil)
        alert.addAction(title: "Pausar reporte", style: .default) { (_) in
            AvisoPorReportar.pausarReporte()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: segueToPerform, sender: nil)
            }
        }
        alert.addAction(title: "Eliminar reporte y salir", style: .destructive) { (_) in
            AvisoPorReportar.clearSharedInstance()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: segueToPerform, sender: nil)
            }
            
        }
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func numeroEspeciesChange(_ sender: Any) {
        var newInt : Int? = Int(txtNumeroEspecimenes.text!)
        
        if newInt != nil {
            if newInt! < 1 {
                newInt = 1
            }
        } else {
            newInt = 1
        }
        AvisoPorReportar.sharedInstance.numeroEspecimenes = newInt!
        if(newInt! < 2 && AvisoPorReportar.sharedInstance.estadoAnimal == .vivosMuertos){
            AvisoPorReportar.sharedInstance.estadoAnimal = .vivo
        }
        loadLabelsFromAviso()
        reloadPickerEstadoEspecimen()
    }
    
    
    
    @IBAction func onEnviarClic(_ sender: Any) {
        enviarReporte()
    }
    
    func enviarReporte () {
        self.btnEnviarReporte.loadingIndicator(true)
        
        switch AppStatus.shared.usageStatus {
        case .fueraDeLinea :
            AvisoPorReportar.pausarReporte()
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Reporte guardado", message: "El reporte se guadó en el dispositivo. Inicia sesión para continuar con el registro.", preferredStyle: .alert)
                alert.addAction(title: "OK", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        self.btnEnviarReporte.loadingIndicator(false)
                        self.performSegue(withIdentifier: "resumenReturnOfflineHomeSegue", sender: nil)
                    }
                })
                self.present(alert, animated: true, completion: nil)
            }
        case .logueado:
            Services.enviarReporteDeVaramiento(token: KeychainWrapper.standard.string(forKey: Config.KeychainKeys.token) ?? "", onComplete: {
                AvisoPorReportar.clearSharedInstance()
                DispatchQueue.main.async {
                    self.btnEnviarReporte.loadingIndicator(false)
                    let alert = UIAlertController(title: "Reporte enviado", message: "Gracias por realizar tu reporte. Éste se ha enviado a las autoridades y en breve será atendido", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: { (_) in
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "resumenReturnHomeSegue", sender: nil)
                        }
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            }) { (err) in
                switch err {
                case .badRequest(let reasons):
                    print(reasons)
                case .unauthorized:
                    self.solicitarLogin()
                case .existingUser, .notFound, .serverSide, .timeOut:
                    DispatchQueue.main.async {
                        self.btnEnviarReporte.loadingIndicator(false)
                        let alert = UIAlertController(title: "Hubo un error en la conexión con el servidor", message: "Verifica tu conexión y vuelve a intentar", preferredStyle: .alert)
                        alert.addAction(title: "OK", style: .default, handler: nil)
                        self.present(alert, animated: true, completion: nil)
                    }
                case .onlyWifiAllowed:
                    DispatchQueue.main.async {
                        self.btnEnviarReporte.loadingIndicator(false)
                        let alert = UIAlertController(title: "Solo Wi-Fi permitido", message: "Puedes cambiar la configuración en la sección de Perfil.", preferredStyle: .alert)
                        alert.addAction(title: "OK", style: .default, handler: nil)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        
    }
    
    
    func solicitarLogin(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "AuthScreens", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            controller.loginDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
}

extension ResumenReporteTableViewController : LoginDelegate {
    func onLogin() {
        enviarReporte()
    }
    
    
}

extension ResumenReporteTableViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        AvisoPorReportar.sharedInstance.indicaciones = txtIndicaciones.text
        AvisoPorReportar.sharedInstance.referenciasLugar = txtReferencias.text
        AvisoPorReportar.sharedInstance.observaciones = txtObservaciones.text
        AvisoPorReportar.sharedInstance.nombreDelLugar = txtNombreLugar.text
        
        loadLabelsFromAviso()
    }
}

extension ResumenReporteTableViewController : UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerSustrato {
            return dataSustrato.count
        }
        if pickerView == pickerFirstSeen {
            return dataFirstSeen.count
        }
        else  { //pickerView == pickerEstadoEspecimen
            return dataEstadoEspecimen.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerSustrato {
            return dataSustrato[row]
        }
        if pickerView == pickerFirstSeen {
            return dataFirstSeen[row]
        }
        else  { //pickerView == pickerEstadoEspecimen
            return dataEstadoEspecimen[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerSustrato {
            switch dataSustrato[row] {
            case "Arena": AvisoPorReportar.sharedInstance.tipoSustrato = .arena
            case "Grava": AvisoPorReportar.sharedInstance.tipoSustrato = .grava
            case "Rocoso": AvisoPorReportar.sharedInstance.tipoSustrato = .rocoso
            default: AvisoPorReportar.sharedInstance.tipoSustrato = .arena
            }
            loadLabelsFromAviso()
        } else if pickerView == pickerFirstSeen {
            AvisoPorReportar.sharedInstance.vistoPorPrimeraVez = LugarDondeSeVio.fromServerId(id: "\(row)")
            loadLabelsFromAviso()
        } else  { //pickerView == pickerEstadoEspecimen
            switch dataEstadoEspecimen[row] {
            case "Vivo":
                AvisoPorReportar.sharedInstance.estadoAnimal = .vivo
                break
            case "muerto":
                AvisoPorReportar.sharedInstance.estadoAnimal = .muerto
                break
            case "Indefinido":
                AvisoPorReportar.sharedInstance.estadoAnimal = .indefinido
                break
            case "Vivos y muertos":
                AvisoPorReportar.sharedInstance.estadoAnimal = .vivosMuertos
                break
            default:
                AvisoPorReportar.sharedInstance.estadoAnimal = .vivo
            }
            
            loadLabelsFromAviso()
        }
    }
    
    
}

extension ResumenReporteTableViewController : SeleccionarEspecieDelegate{
    func onEspecieDefinida(especie: TipoAnimal) {
        AvisoPorReportar.sharedInstance.tipoAnimal = especie
        self.loadLabelsFromAviso()
    }
}

extension ResumenReporteTableViewController : FotografiasDelegate {
    func onFotografiasEnd() {
        loadLabelsFromAviso()
    }
}

extension ResumenReporteTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                handleFotoClic()
            default:break
            }
        case 1:
            switch indexPath.row {
            case 1...2:
                handleLocalizacion()
            case 5:
                handleEspecie()
            default:break
            }
        default : break
        }
    }
    
    func handleFotoClic(){
        self.performSegue(withIdentifier: "resumenFotografiasSegue", sender: nil)
    }
    
    func handleLocalizacion(){
        
        switch AppStatus.shared.usageStatus {
        case .fueraDeLinea:
            let alert = UIAlertController(title: "Inicia sesión", message: "Para modificar la posición del varamiento pausa el reporte actual (en la X de la esquina derecha) e inicia sesión.", preferredStyle: .alert)
            alert.addAction(title: "OK", style: .default, handler: nil)
            self.present(alert, animated: true, completion: nil)
        case .logueado:
            let storyBoard: UIStoryboard = UIStoryboard(name: "LocationPicker", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "locationPickerNav") as! UINavigationController
            let locationPicker = vc.topViewController as! LocationPickerViewController
            locationPicker.delegate = self
            locationPicker.posicionVaramiento = AvisoPorReportar.sharedInstance.coordenadas
            DispatchQueue.main.async {
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    func handleEspecie(){
        self.performSegue(withIdentifier: "identificarEspecieResumenSegue", sender: nil)
    }
}

extension ResumenReporteTableViewController : LocationPickerDelegate {
    func onLocationPicked(_ position: CLLocationCoordinate2D) {
        AvisoPorReportar.sharedInstance.coordenadas = position
        self.loadLabelsFromAviso()
    }
    
    func didCancel() {
        
    }
    
    
}
