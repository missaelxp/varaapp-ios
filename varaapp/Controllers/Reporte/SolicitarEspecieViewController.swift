//
//  SolicitarEspecieViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 02/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class SolicitarEspecieViewController: UIViewController {

    @IBOutlet weak var btnContinuar: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "identificarEspecieSegue"{
            let navController = segue.destination as! UINavigationController
            let vc = navController.topViewController as! SeleccionarEspecieViewController
            vc.especieDelegate = self as SeleccionarEspecieDelegate
        }
    }
    
}

extension SolicitarEspecieViewController : SeleccionarEspecieDelegate {
    func onEspecieDefinida(especie: TipoAnimal) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "showResumenSegue", sender: nil)
        }
        
    }
}
