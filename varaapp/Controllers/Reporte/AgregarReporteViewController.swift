//
//  AgregarReporteViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 22/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import Lottie
import CoreLocation

class AgregarReporteViewController: UIViewController {

    @IBOutlet weak var loadingViewContainer: UIView!
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var btnEstablecerManualmente: UIButton!
    
    let locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppStatus.shared.usageStatus == .fueraDeLinea {
            btnEstablecerManualmente.isHidden = true
        }
        
        let animationView = LOTAnimationView(name: "ola2")
        animationView.contentMode = .scaleAspectFit
        animationView.loopAnimation = true
        
        loadingViewContainer.addSubview(animationView)
        
        // Auto layout
        animationView.translatesAutoresizingMaskIntoConstraints = false
        
        animationView.topAnchor.constraint(equalTo: loadingViewContainer.topAnchor).isActive = true
        animationView.bottomAnchor.constraint(equalTo: loadingViewContainer.bottomAnchor).isActive = true
        animationView.leadingAnchor.constraint(equalTo: loadingViewContainer.leadingAnchor).isActive = true
        animationView.trailingAnchor.constraint(equalTo: loadingViewContainer.trailingAnchor).isActive = true
        
        animationView.play()
        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }

    @IBAction func onCancelarClic(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func onManualClic(_ sender: Any) {

        let storyBoard: UIStoryboard = UIStoryboard(name: "LocationPicker", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "locationPickerNav") as! UINavigationController
        let locationPicker = vc.topViewController as! LocationPickerViewController
        locationPicker.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    

}

extension AgregarReporteViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        AvisoPorReportar.sharedInstance.coordenadas = locations[0].coordinate
        locationManager.stopUpdatingLocation()
        
        performSegue(withIdentifier: "showResultPositionSegue", sender: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Fue imposible acceder a tu posición", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(title: "Seleccionar manualmente", style: .default) { (_) in
                print("LLamando a controlador manual");
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    
}

extension AgregarReporteViewController : LocationPickerDelegate {
    func didCancel() {
        
    }
    
    func onLocationPicked(_ position: CLLocationCoordinate2D) {
        AvisoPorReportar.sharedInstance.coordenadas = position
        self.performSegue(withIdentifier: "showResultPositionSegue", sender: nil)
    }
    
    
}
