//
//  FotografiasViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 01/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

protocol FotografiasDelegate {
    func onFotografiasEnd()
}

class FotografiasViewController: UIViewController {
    
    @IBOutlet weak var tableViewFotos: UITableView!
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var btnAgregarFoto: UIButton!
    private var imagePicker : UIImagePickerController!
    var fotografiasDelegate : FotografiasDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fotografías"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.varaapp.blue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        btnAgregarFoto.layer.cornerRadius = btnAgregarFoto.frame.height / 2
        btnAgregarFoto.layer.borderWidth = 1
        btnAgregarFoto.layer.borderColor = UIColor.varaapp.lightGreen.cgColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AvisoPorReportar.getFotografiasTotalSizeInMB() > Services.maxRequestSize {
            AvisoPorReportar.sharedInstance.fotografias.removeLast()
            tableViewFotos.reloadData()
        }
    }
    
    @IBAction func onAgregarFotoClic(_ sender: Any) {
        imagePicker = UIImagePickerController();
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.varaapp.blue // Background color
        imagePicker.navigationBar.tintColor = UIColor.white // Cancel button ~ any UITabBarButton items
        
        
        imagePicker.delegate = self
        
        let alert = UIAlertController(title: "Fuente de archivo", message: "Selecciona el origen de la fotografía", preferredStyle: .actionSheet)
        alert.addAction(title: "Cámara", style: .default) { (_) in
            //camara seleccionada
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                self.imagePicker.sourceType = .camera
            } else {
                self.imagePicker.sourceType = .photoLibrary
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        alert.addAction(title: "Galería", style: .default) { (_) in
            //Galeria seleccionada
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func onContinuarCic(_ sender: Any) {
        if(AvisoPorReportar.sharedInstance.fotografias.count > 0){
            //self.performSegue(withIdentifier: "estadoAnimalSegue", sender: nil)
            self.fotografiasDelegate.onFotografiasEnd()
            self.dismiss(animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "No hay imágenes en el reporte", message: "Agrega por lo menos una fotografía a tu reporte", preferredStyle: .alert)
            alert.addAction(title: "OK", style: .default, handler: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    

}
extension FotografiasViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AvisoPorReportar.sharedInstance.fotografias.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let aviso = avisos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "fotoCell") as! FotografiaCell
        cell.imagen.image = AvisoPorReportar.sharedInstance.fotografias[indexPath.row]
        return cell
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let deleteRowAction = UITableViewRowAction(style: .default, title: "Descartar imagen", handler:{action, indexpath in
            AvisoPorReportar.sharedInstance.fotografias.remove(at: indexPath.item)
            self.tableViewFotos.reloadData()
        });
        
        return [deleteRowAction];
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}

extension FotografiasViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if AvisoPorReportar.getFotografiasTotalSizeInMB() > Services.maxRequestSize {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Tamaño de reporte excedido", message: "El tamaño de las imagenes incluidas en el reporte supera lo permitido. Por favor incluye únicamente las imágenes necesarias.", preferredStyle: .alert)
                alert.addAction(title: "OK", style: .default, handler: nil)
                self.present(alert, animated: true, completion: nil)
            }
            dismiss(animated: true, completion: nil)
            return
        }
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            AvisoPorReportar.sharedInstance.fotografias.append(pickedImage);
        }
        dismiss(animated: true, completion: nil)
        tableViewFotos.reloadData()
    }
}
