//
//  PrimeraVezVistoEnViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/28/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import DLRadioButton

class PrimeraVezVistoEnViewController: UIViewController {
    @IBOutlet weak var btnPlaya: DLRadioButton!
    @IBOutlet weak var btnAgua: DLRadioButton!
    @IBOutlet weak var btnContinuar: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAgua.setBottomBorder(color: UIColor.varaapp.lightGreen)
        btnPlaya.setBottomBorder(color: UIColor.varaapp.lightGreen)
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
    }
    override func viewDidAppear(_ animated: Bool) {
        switch AvisoPorReportar.sharedInstance.vistoPorPrimeraVez {
        case .agua:
            btnAgua.isSelected = true
        case .playa:
            btnPlaya.isSelected = true
        }
    }
    
    
    @IBAction func onPlayaClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.vistoPorPrimeraVez = .playa
    }
    
    @IBAction func onAguaClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.vistoPorPrimeraVez = .agua
    }
    
    
}
