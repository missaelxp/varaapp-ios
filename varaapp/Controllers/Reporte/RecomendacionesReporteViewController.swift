//
//  RecomendacionesReporteViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 01/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import MobileCoreServices

class RecomendacionesReporteViewController: UIViewController {
    @IBOutlet weak var btnTomarFotos: UIButton!
    
    private var imagePicker : UIImagePickerController!
    @IBOutlet weak var scrolllView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTomarFotos.layer.cornerRadius = btnTomarFotos.frame.height / 2
        
        scrolllView.delegate = self
        
        let imageView = UIImageView(image: UIImage(named: "infografia modificada"))
        let imageProporcion = Double(5158.0/1563.0)
        var resultHeight = Double(view.frame.width)
        resultHeight = resultHeight * imageProporcion
        imageView.frame = CGRect(x: scrolllView.frame.minX, y: scrolllView.frame.minY, width: view.frame.width, height: CGFloat(resultHeight))
        scrolllView.addSubview(imageView)
        scrolllView.contentSize = CGSize(width: Double(scrolllView.frame.width), height: resultHeight  + 70)
        self.btnTomarFotos.layer.opacity = 0
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fotografiasReporteSegue" {
            let navController = segue.destination as! UINavigationController
            let vc = navController.topViewController as! FotografiasViewController
            vc.fotografiasDelegate = self
        }
    }
    
    @IBAction func takePhotoClic(_ sender: Any) {
        imagePicker = UIImagePickerController();
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.varaapp.blue // Background color
        imagePicker.navigationBar.tintColor = UIColor.white // Cancel button ~ any UITabBarButton items
        
        
        imagePicker.delegate = self
        
        
        let alert = UIAlertController(title: "Fuente de archivo", message: "Selecciona el origen de la fotografía", preferredStyle: .actionSheet)
        alert.addAction(title: "Cámara", style: .default) { (_) in
            //camara seleccionada
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                self.imagePicker.sourceType = .camera
            } else {
                self.imagePicker.sourceType = .photoLibrary
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        alert.addAction(title: "Galería", style: .default) { (_) in
            //Galeria seleccionada
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        alert.addAction(title: "Cancelar", style: .cancel, handler: nil)
        
        self.present(alert, animated: true, completion: nil)
    }

}

extension RecomendacionesReporteViewController : FotografiasDelegate {
    func onFotografiasEnd() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "estadoEspecimenesSegue", sender: nil)
        }
    }
}

extension RecomendacionesReporteViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            AvisoPorReportar.sharedInstance.fotografias.append(pickedImage);
        }
        dismiss(animated: true, completion: nil)
        
        self.performSegue(withIdentifier: "fotografiasReporteSegue", sender: nil)
    }
}


extension RecomendacionesReporteViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + 30 >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            self.btnTomarFotos.layer.opacity = 1
        } else {
            self.btnTomarFotos.layer.opacity = 0
        }
    }
}
