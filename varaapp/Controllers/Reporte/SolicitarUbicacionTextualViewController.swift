//
//  SolicitarUbicacionTextualViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 22/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class SolicitarUbicacionTextualViewController: UIViewController {
    @IBOutlet weak var btnContinuar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
