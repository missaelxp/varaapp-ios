//
//  ReportesViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 22/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftKeychainWrapper

let reportesCache = NSCache<AnyObject, AnyObject>()

class ReportesViewController: UIViewController {
    @IBOutlet weak var avisosTableView: UITableView!
    
    var avisos : [AvisoVaramiento] = []
    var views : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadReportesFromServer()
        avisosTableView.delegate = self
        avisosTableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.title = "Varamientos"
    }
    
    func loadReportesFromServer(){
        if let reportesFromCache = reportesCache.object(forKey: "reportesCache" as AnyObject) as? [AvisoVaramiento] {
            self.avisos = reportesFromCache
            self.avisosTableView.reloadData()
        }
        Services.obtenerVaramientos(token: KeychainWrapper.standard.string(forKey: Config.KeychainKeys.token) ?? "", onComplete: { (varamientos) in
            self.avisos = varamientos
            reportesCache.setObject(varamientos as AnyObject, forKey: "reportesCache" as AnyObject)
            DispatchQueue.main.async {
                self.avisosTableView.reloadData()
            }
        }) { (error) in
            switch error {
            case .unauthorized:
                self.requestLogin()
            case .badRequest(_),
                 .existingUser,
                 .notFound,
                 .serverSide,
                 .timeOut:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Hubo un error al conectar con el servidor", message: "Revisa tu conexión", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(title: "Reintentar", style: UIAlertAction.Style.cancel, handler: { (_) in
                        self.loadReportesFromServer()
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            case .onlyWifiAllowed:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Solo se permite el uso de red Wi-Fi.", message: "Cambia la configuración en la sección de Perfil.", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    func requestLogin(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "AuthScreens", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            controller.loginDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension ReportesViewController : LoginDelegate {
    func onLogin() {
        self.loadReportesFromServer()
    }
}

extension ReportesViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return avisos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aviso = avisos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvisoVaramientoCell") as! ReportesCell
        cell.setAviso(aviso: aviso)
        return cell
        
    }
    // El siguiente codigo es para detener el seguimiento de varamientos pero no se entregará
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//
//
//        let deleteRowAction = UITableViewRowAction(style: .default, title: "Detener seguimiento", handler:{action, indexpath in
//            print("DELETE•ACTION");
//        });
//
//        return [deleteRowAction];
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reporteSeleccionado = avisos[indexPath.item]
        let storyBoard = UIStoryboard(name: "MainReporte", bundle: nil)
        let detailController = storyBoard.instantiateViewController(withIdentifier: "ReportesDetail") as! ReportesDetailViewController
        
        detailController.setAviso(reporteSeleccionado)
        self.navigationController?.pushViewController(detailController, animated: true)
        
    }
    
}
