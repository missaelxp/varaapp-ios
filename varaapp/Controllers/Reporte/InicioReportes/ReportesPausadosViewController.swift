//
//  ReportesPausadosViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/17/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class ReportesPausadosViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var avisos : [Varamiento] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getVaramientosFromCoreData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.title = "Reportes pausados"
    }
    
    func getVaramientosFromCoreData(){
        var varamientos : [Varamiento] = CoreDataManager.findAll("Varamiento") as! [Varamiento]
        varamientos.sort(by: {($0.fecha! as Date) < ($1.fecha! as Date)})
        self.avisos = varamientos
        tableView.reloadData()
    }

}

extension ReportesPausadosViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return avisos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aviso = avisos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvisoPausadoCell") as! ReportesPausadosCell
        cell.setAviso(aviso: aviso)
        return cell
        
    }
    
        func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    
    
            let deleteRowAction = UITableViewRowAction(style: .default, title: "Eliminar reporte", handler:{action, indexpath in
                CoreDataManager.context.delete(self.avisos[indexPath.row])
                do{
                    try CoreDataManager.save()
                } catch {
                    
                }
                self.getVaramientosFromCoreData()
            });
    
            return [deleteRowAction];
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reporteSeleccionado = avisos[indexPath.item]
        
        AvisoPorReportar.setFromCoreData(aviso: reporteSeleccionado)
        
        self.performSegue(withIdentifier: "reportePausadoToResumenSegue", sender: nil)
        
    }
    
    
}
