//
//  PageControllerFotografiasReporte.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/18/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class PageControllerFotografiasReporte: UIPageViewController {
    
    var imageUrls : [String] = []
    
    var orderedViewControllers : [UIViewController] = []
    
    var pageControl = UIPageControl()
    var btnBack = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        
        
        createVCFromImageURLs()
        
        if let firstViewController = orderedViewControllers.first{
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        if  let window = (UIApplication.shared.delegate?.window)! as UIWindow? {
            window.backgroundColor = UIColor.white
        }
        configurePageControl()
        configureBackButton()
    }
    func createVCFromImageURLs(){
        for url in imageUrls {
            orderedViewControllers.append(self.newVc(url: url))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //pageControl.customPageControl(dotFillColor: UIColor.varaapp.blue, dotBorderColor: UIColor.varaapp.blue, dotBorderWidth: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        if (self.isMovingFromParentViewController) {
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    }
     @objc func canRotate() -> Void {}
    
    override func viewDidLayoutSubviews() {
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }
    }
    
    func newVc(url : String) -> UIViewController {
        let vc = UIStoryboard.init(name: "MainReporte", bundle: nil).instantiateViewController(withIdentifier: "fullScreenFotografiaVC") as! SingleFotografiaViewController
        vc.setImage(url: url)
        return vc
    }
    
    func configurePageControl (){
        //pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        pageControl = UIPageControl()
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        //        pageControl.tintColor = UIColor.varaapp.blue
        //        pageControl.pageIndicatorTintColor = UIColor.varaapp.blue
        //        pageControl.currentPageIndicatorTintColor = UIColor.varaapp.yellow
        
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: UIColor.varaapp.blue)
        self.pageControl.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        self.pageControl.currentPageIndicatorTintColor = UIColor.varaapp.blue
        
        pageControl.isUserInteractionEnabled = false
        
        self.view.addSubview(pageControl)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -5).isActive = true
        pageControl.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    func configureBackButton(){
        btnBack = UIButton(frame: CGRect(x: UIScreen.main.bounds.minX + 3, y: view.frame.minY + 20, width: 50, height: 50))
        
        let origImage = #imageLiteral(resourceName: "exit")
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(tintedImage, for: .normal)
        btnBack.setTitleColor(UIColor.varaapp.blue, for: .normal)
        btnBack.tintColor = UIColor.varaapp.blue
        
        btnBack.addTarget(self, action: #selector(self.btnBackClic(_:)), for: .touchUpInside)
        
        self.view.addSubview(btnBack)
    }
    @objc func btnBackClic(_ sender:UIButton!)
    {
        UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension PageControllerFotografiasReporte :  UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {return nil}
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            //return orderedViewControllers.last
            //descomentar lo siguiente oara no hacerlo infinito
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex ]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {return nil}
        let nextIndex = viewControllerIndex + 1
        
        guard  orderedViewControllers.count != nextIndex else {
            //return orderedViewControllers.first
            //descomentar lo siguiente oara no hacerlo infinito
            return nil
        }
        
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex ]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let pageContentViewController = pageViewController.viewControllers![0]
        
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
}
