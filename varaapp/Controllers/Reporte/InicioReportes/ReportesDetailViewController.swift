//
//  ReportesDetailViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 29/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ReportesDetailViewController: UITableViewController {
    
    private var aviso : AvisoVaramiento = AvisoVaramiento()
    
    @IBOutlet weak var imgPrincipal: UIImageView!
    @IBOutlet weak var lbIdReporte: UILabel!
    @IBOutlet weak var lbFecha: UILabel!
    @IBOutlet weak var lbLatitud: UILabel!
    @IBOutlet weak var lbLongitud: UILabel!
    @IBOutlet weak var lbNumeroEspecimenes: UILabel!
    @IBOutlet weak var lbEstadoEspecimen: UILabel!
    @IBOutlet weak var lbSustrato: UILabel!
    @IBOutlet weak var lbFacilAcceso: UILabel!
    @IBOutlet weak var lbAcantilado: UILabel!
    @IBOutlet weak var lbFirstSeen: UILabel!
    @IBOutlet weak var lbEspecie: UILabel!
    
    
    
    @IBOutlet weak var txtObservaciones: UITextView!
    @IBOutlet weak var txtLocalizacionInfo: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getReporteFromServer()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgPrincipal.isUserInteractionEnabled = true
        imgPrincipal.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if aviso.fotografiasURLs.count > 0 {
            performSegue(withIdentifier: "showFotosVaramientoSegue", sender: nil)
        } else {
            let alert = UIAlertController(title: "Sin fotografías", message: "Este reporte no contiene fotografías", preferredStyle: .alert)
            alert.addAction(title: "OK", style: .default, handler: nil)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFotosVaramientoSegue" {
            let vc = segue.destination as! PageControllerFotografiasReporte
            vc.imageUrls = aviso.fotografiasURLs
        }
    }
    func getReporteFromServer(){
        guard let idAviso = aviso.id else {return}
        Services.obtenerVaramiento(token: KeychainWrapper.standard.string(forKey: Config.KeychainKeys.token) ?? "", idVaramiento: idAviso, onComplete: { (aviso) in
            self.aviso = aviso
            DispatchQueue.main.async {
                self.setUI()
            }
        }) { (error) in
            switch(error){
            case .badRequest, .existingUser,.notFound,.serverSide,.timeOut,.unauthorized:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error al recuperar archivo", message: "Revisa tu conexión y vuelve a intentarlo", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    
                    alert.addAction(title: "Reintentar", style: .destructive, handler:  { (_) in
                        self.getReporteFromServer()
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            case .onlyWifiAllowed:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Solo Wi-Fi permitido", message: "Cambia los ajustes en la sección de Perfil", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
    }

    func setUI(){
        if aviso.fotografiasURLs.count > 0 {
            let imageLink = "\(ApiEndpoint.baseURL)\(aviso.fotografiasURLs[0])"
            imgPrincipal.downloadedFrom(url: imageLink)
        } else {
            imgPrincipal.image = #imageLiteral(resourceName: "FOTO_reporte_sin_fotos")
        }
        
        if let id = aviso.id{
            lbIdReporte.text = "\(id)"
        } else {
            lbIdReporte.text = "Sin ID"
        }
        
        lbFecha.text = aviso.fecha?.getPrintFecha(format: "dd/MM/yyyy")
        if let latitud = aviso.coordenadas?.latitude{
            lbLatitud.text = "\(latitud.representacionEnGrados(esLatitud: true))"
        } else {
            lbLatitud.text = ""
        }
        
        if let longitud = aviso.coordenadas?.longitude{
            lbLongitud.text = "\(longitud.representacionEnGrados(esLatitud: false))"
        } else {
            lbLongitud.text = ""
        }
        
        
        lbNumeroEspecimenes.text = "\(aviso.numeroEspecimenes)"
        
        self.lbEstadoEspecimen.text = aviso.estadoAnimal.description
        self.lbSustrato.text = aviso.tipoSustrato.description
        self.lbFacilAcceso.text = aviso.facilAcceso ? "Si" : "No"
        self.lbAcantilado.text = aviso.acantilado ? "Si" : "No"
        self.lbFirstSeen.text = aviso.vistoPorPrimeraVez.description
        self.lbEspecie.text = aviso.tipoAnimal.description
        
        self.txtLocalizacionInfo.text = aviso.nombreDelLugar
        
        
        txtObservaciones.text = aviso.observaciones
    }
    
    /// Define el aviso que se mostrará en la pantalla de Avisos
    ///
    /// - Parameter reporte: Aviso a deplegar
    func setAviso(_ reporte: AvisoVaramiento){
        self.aviso = reporte
    }

}
