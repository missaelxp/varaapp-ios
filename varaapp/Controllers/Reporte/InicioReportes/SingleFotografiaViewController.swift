//
//  SingleFotografiaViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/18/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class SingleFotografiaViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imageLink : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.downloadedFrom(url: imageLink)
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        scrollView.delegate = self
    }
    
    func setImage(url: String){
        self.imageLink = "\(ApiEndpoint.baseURL)\(url)"
    }
    
}

extension SingleFotografiaViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
