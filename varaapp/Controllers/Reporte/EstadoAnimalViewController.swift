//
//  EstadoAnimalViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 02/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import DLRadioButton

class EstadoAnimalViewController: UIViewController {
    @IBOutlet weak var btnVivo: DLRadioButton!
    @IBOutlet weak var btnMuerto: DLRadioButton!
    @IBOutlet weak var btnIndefinido: DLRadioButton!
    @IBOutlet weak var btnVivosMuertos: DLRadioButton!
    
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var txtNumeroEspecimenes: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        txtNumeroEspecimenes.text = "\(AvisoPorReportar.sharedInstance.numeroEspecimenes)"
        
        //txtNumeroEspecimenes.setTopBorder(color: UIColor.varaapp.lightGreen)
        txtNumeroEspecimenes.setBottomBorder(color: UIColor.varaapp.lightGreen)
        
        switch AvisoPorReportar.sharedInstance.estadoAnimal {
        case .indefinido:
            btnIndefinido.isSelected = true
        case .muerto:
            btnMuerto.isSelected = true
        case .vivo:
            btnVivo.isSelected = true
        case .vivosMuertos:
            btnVivosMuertos.isSelected = true
        }
        if let numeroEspecimenes : Int = Int(txtNumeroEspecimenes.text!){
            if(numeroEspecimenes > 1){
                btnVivosMuertos.isEnabled = true
            } else {
                btnVivosMuertos.isEnabled = false
            }
        }
        
        btnVivo.setBottomBorder(color: UIColor.varaapp.lightGreen)
        btnMuerto.setBottomBorder(color: UIColor.varaapp.lightGreen)
        btnIndefinido.setBottomBorder(color: UIColor.varaapp.lightGreen)
        btnVivosMuertos.setBottomBorder(color: UIColor.varaapp.lightGreen)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onVivoClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.estadoAnimal = .vivo
    }
    @IBAction func OnEditarNumeroEspecimenes(_ sender: Any) {
        if let numeroEspecimenes : Int = Int(txtNumeroEspecimenes.text!){
            if(numeroEspecimenes <= 0){
                txtNumeroEspecimenes.text = "1"
                AvisoPorReportar.sharedInstance.numeroEspecimenes = 1
            } else if(numeroEspecimenes == 1){
                btnVivo.isSelected = true
                btnVivosMuertos.isEnabled = false
                AvisoPorReportar.sharedInstance.numeroEspecimenes = 1
            } else if numeroEspecimenes > 1000{
                txtNumeroEspecimenes.text = "1"
                AvisoPorReportar.sharedInstance.numeroEspecimenes = 1
            } else {
                btnVivosMuertos.isEnabled = true
                AvisoPorReportar.sharedInstance.numeroEspecimenes = numeroEspecimenes
            }
        } else {
            txtNumeroEspecimenes.text = "1"
            AvisoPorReportar.sharedInstance.numeroEspecimenes = 1
        }
    }
    
    @IBAction func onMuertoClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.estadoAnimal = .muerto
    }
    @IBAction func onIndefinidoClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.estadoAnimal = .indefinido
    }
    @IBAction func onHayVivoYMuertosClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.estadoAnimal = .vivosMuertos
    }
    
}
