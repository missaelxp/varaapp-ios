//
//  DatosVaramientoViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 22/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import DLRadioButton

class DatosVaramientoViewController: UIViewController {
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var btnArena: DLRadioButton!
    @IBOutlet weak var btnGrava: DLRadioButton!
    @IBOutlet weak var btnRocoso: DLRadioButton!
    
    @IBOutlet weak var viewBtnArena: UIView!
    @IBOutlet weak var viewBtnGrava: UIView!
    @IBOutlet weak var viewBtnRocoso: UIView!
    
    @IBOutlet weak var SCfacilAccesso: UISegmentedControl!
    @IBOutlet weak var SCAcantilado: UISegmentedControl!
    
    @IBOutlet weak var viewFacilAcceso: UIView!
    @IBOutlet weak var viewAcantilado: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        
        viewFacilAcceso.addBottomBorder(color: UIColor.varaapp.lightGreen)
        viewAcantilado.addBottomBorder(color: UIColor.varaapp.lightGreen)
        
        viewBtnArena.addBottomBorder(color: UIColor.varaapp.lightGreen)
        viewBtnGrava.addBottomBorder(color: UIColor.varaapp.lightGreen)
        viewBtnRocoso.addBottomBorder(color: UIColor.varaapp.lightGreen)
        
        
        
//        SCfacilAccesso.backgroundColor = .clear
//        SCAcantilado.backgroundColor = .clear
//        SCfacilAccesso.tintColor = .clear
//        SCAcantilado.tintColor = .clear
        
//        SCfacilAccesso.setTitleTextAttributes([
//            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17),
//            NSAttributedStringKey.foregroundColor: UIColor.gray
//            ], for: .normal)
//        SCfacilAccesso.setTitleTextAttributes([
//            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17),
//            NSAttributedStringKey.foregroundColor: UIColor.varaapp.blue
//            ], for: .selected)
//        SCAcantilado.setTitleTextAttributes([
//            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17),
//            NSAttributedStringKey.foregroundColor: UIColor.gray
//            ], for: .normal)
//        SCAcantilado.setTitleTextAttributes([
//            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17),
//            NSAttributedStringKey.foregroundColor: UIColor.varaapp.blue
//            ], for: .selected)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        switch AvisoPorReportar.sharedInstance.tipoSustrato {
        case .arena:
            btnArena.isSelected = true
        case .grava:
            btnGrava.isSelected = true
        case .rocoso:
            btnRocoso.isSelected = true
        }
    }

    
    @IBAction func onBtnArena(_ sender: Any) {
        AvisoPorReportar.sharedInstance.tipoSustrato = .arena
    }
    
    @IBAction func onBtnGrava(_ sender: Any) {
        AvisoPorReportar.sharedInstance.tipoSustrato = .grava
    }
    @IBAction func onBtnRocoso(_ sender: Any) {
        AvisoPorReportar.sharedInstance.tipoSustrato = .rocoso
    }
    
    
    @IBAction func onEsFacilAcceso(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex     {
        case 0:
            AvisoPorReportar.sharedInstance.facilAcceso = true
        case 1:
            AvisoPorReportar.sharedInstance.facilAcceso = false
        default:
            break
        }
    }
    @IBAction func onEsAcantiladoChange(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex     {
        case 0:
            AvisoPorReportar.sharedInstance.acantilado = true
        case 1:
            AvisoPorReportar.sharedInstance.acantilado = false
        default:
            break
        }
    }
    


}
