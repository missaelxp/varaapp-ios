//
//  InformacionAdicionalTableViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 22/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class InformacionAdicionalTableViewController: UITableViewController {
    @IBOutlet weak var txtNombreLugar: UITextField!
    @IBOutlet weak var txtIndicaciones: UITextView!
    @IBOutlet weak var txtReferencias: UITextView!
    @IBOutlet weak var txtObservaciones: UITextView!
    @IBOutlet var tableViewAdicional: UITableView!
    
    @IBOutlet weak var btnContinuar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        self.tableViewAdicional.separatorColor = UIColor.groupTableViewBackground;
        
        txtNombreLugar.text = AvisoPorReportar.sharedInstance.nombreDelLugar
        txtIndicaciones.text = AvisoPorReportar.sharedInstance.indicaciones
        txtReferencias.text = AvisoPorReportar.sharedInstance.referenciasLugar
        txtObservaciones.text = AvisoPorReportar.sharedInstance.observaciones
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onContinuarClic(_ sender: Any) {
        AvisoPorReportar.sharedInstance.nombreDelLugar = txtNombreLugar.text!
        AvisoPorReportar.sharedInstance.indicaciones = txtIndicaciones.text
        AvisoPorReportar.sharedInstance.referenciasLugar = txtReferencias.text
        AvisoPorReportar.sharedInstance.observaciones = txtObservaciones.text
    }
}

extension InformacionAdicionalTableViewController : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        return updatedText.count <= 100
    }
}
