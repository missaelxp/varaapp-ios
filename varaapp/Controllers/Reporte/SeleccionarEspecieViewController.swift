//
//  SeleccionarEspecieViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 02/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

protocol SeleccionarEspecieDelegate {
    func onEspecieDefinida(especie: TipoAnimal)
}

class SeleccionarEspecieViewController: UIViewController {
    var especieDelegate : SeleccionarEspecieDelegate!
    @IBOutlet weak var btnNinguno: UIButton!
    @IBOutlet weak var tableViewEspecie: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.varaapp.blue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        btnNinguno.layer.cornerRadius = btnNinguno.frame.height / 2
        
        
        
        //let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableViewEspecie.frame.size.width, height: 210))
        let label = UIButton(frame: CGRect(x: 0, y: 0, width: tableViewEspecie.frame.size.width, height: 210))
        label.contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 100, right: 12)
        label.setTitle("Imágenes obtenidas de: Jefferson, T.A., S. Leatherwood, and M.A. Webber FAO species identification guide. Marine mammals of the world. Rome, FAO. 1993. 320 p.", for: .normal)
        
        
        label.isUserInteractionEnabled = false
        label.tintColor = .black
        label.titleLabel?.lineBreakMode = .byWordWrapping
        label.titleLabel?.textAlignment = .justified
        label.titleLabel?.numberOfLines = 0
        label.setTitleColor(UIColor.black, for: .normal)
        label.titleLabel?.font =  .systemFont(ofSize: 12)
        //label.backgroundColor = .red
        
        
        tableViewEspecie.tableFooterView = label
        
    }

    
    
    
    @IBAction func onHomeClic(_ sender: Any) {
        let segueToPerform = AppStatus.shared.usageStatus == .fueraDeLinea ? "segueReturnOfflineHomeSeleccionarEspecie" : "segueReturnHomeSeleccionarEspecie"
        
        let alert = UIAlertController(title: "Estas a punto de salir", message: "¿Deseas pausar el reporte?", preferredStyle: .alert)
        alert.addAction(title: "Continuar reporte", style: .default, handler: nil)
        alert.addAction(title: "Pausar reporte", style: .default) { (_) in
            AvisoPorReportar.pausarReporte()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: segueToPerform, sender: nil)
            }
        }
        alert.addAction(title: "Eliminar reporte y salir", style: .destructive) { (_) in
            AvisoPorReportar.clearSharedInstance()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: segueToPerform, sender: nil)
            }
            
        }
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "especieSeleccionadaDesdeFoto" {
            let vc = segue.destination as! EspecieIdentificadaViewController
            vc.seleccionarEspecieDelegate = self.especieDelegate
        } else if segue.identifier == "showPreguntasDicotomicasSegue"{
            let vc = segue.destination as! PreguntasDicotomicasViewController
            vc.seleccionarEspecieDelegate = self.especieDelegate
        }
        
    }
}

extension SeleccionarEspecieViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Especie.lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "especieCell") as! EspeciesCell
        cell.imageEspecie.image = Especie.lista[indexPath.item].1
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AvisoPorReportar.sharedInstance.tipoAnimal = Especie.lista[indexPath.item].0
        self.performSegue(withIdentifier: "especieSeleccionadaDesdeFoto", sender: nil)
    }
}
