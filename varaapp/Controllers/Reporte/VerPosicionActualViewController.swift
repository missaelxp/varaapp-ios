//
//  VerPosicionActualViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 01/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import MapKit
//posicionreturnHomeSegue
//posicionreturnOfflineHomeSegue

class VerPosicionActualViewController: UIViewController {
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var btnEstablecerManualmente: UIButton!
    
    @IBOutlet weak var mapaMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppStatus.shared.usageStatus == .fueraDeLinea{
            btnEstablecerManualmente.isHidden = true
            self.performSegue(withIdentifier: "posicionManualSegue", sender: nil)
        }
        
        btnContinuar.layer.cornerRadius = btnContinuar.frame.height / 2
        btnEstablecerManualmente.layer.cornerRadius = btnEstablecerManualmente.frame.height / 2
        btnEstablecerManualmente.layer.borderWidth = 1
        btnEstablecerManualmente.layer.borderColor = UIColor.varaapp.lightGreen.cgColor
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = AvisoPorReportar.sharedInstance.coordenadas
        mapaMapView.addAnnotation(annotation)
        
        let latDelta:Double = 0.001
        let lngDelta:Double = 0.001
        let locationcoordinates = AvisoPorReportar.sharedInstance.coordenadas
        let zoomSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lngDelta)
        let region = MKCoordinateRegion(center: locationcoordinates, span: zoomSpan)
        self.mapaMapView.setRegion(region, animated: true)
        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onUbicacionManualmenteClic(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LocationPicker", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "locationPickerNav") as! UINavigationController
        let locationPicker = vc.topViewController as! LocationPickerViewController
        locationPicker.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onCancelarClic(_ sender: Any) {
        if AppStatus.shared.usageStatus == .fueraDeLinea {
            self.performSegue(withIdentifier: "posicionreturnOfflineHomeSegue", sender: nil)
        } else {
            self.performSegue(withIdentifier: "posicionreturnHomeSegue", sender: nil)
        }
    }
    
}

extension VerPosicionActualViewController : LocationPickerDelegate{
    func didCancel() {
        
    }
    
    func onLocationPicked(_ position: CLLocationCoordinate2D) {
        self.performSegue(withIdentifier: "posicionManualSegue", sender: nil)
    }
    
    
}
