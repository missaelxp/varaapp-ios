//
//  PreguntasDicotomicasViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 29/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit

class PreguntasDicotomicasViewController: UIViewController {
    @IBOutlet weak var lbPregunta: UILabel!
    @IBOutlet weak var lbRespuesta1: UILabel!
    @IBOutlet weak var lbRespuesta2: UILabel!
    
    @IBOutlet weak var viewRespuesta1: UIView!
    @IBOutlet weak var viewRespuesta2: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var arrow1: UIImageView!
    @IBOutlet weak var arrow2: UIImageView!
    
    @IBOutlet weak var btnReiniciar: UIButton!
    
    var seleccionarEspecieDelegate : SeleccionarEspecieDelegate!
    
    
    //definicion de preguntas, unicamente los apuntadores
    //Para ver la nomenclatura vease preguntas dicotomicas-swift.docx
    //Preguntas finales
    var pf1 : PreguntaDicotomica = PreguntaDicotomica()
    var pf2 : PreguntaDicotomica = PreguntaDicotomica()
    var pf3 : PreguntaDicotomica = PreguntaDicotomica()
    var pf4 : PreguntaDicotomica = PreguntaDicotomica()
    var pf5 : PreguntaDicotomica = PreguntaDicotomica()
    var pf6 : PreguntaDicotomica = PreguntaDicotomica()
    var pf7 : PreguntaDicotomica = PreguntaDicotomica()
    var pf8 : PreguntaDicotomica = PreguntaDicotomica()
    var pf9 : PreguntaDicotomica = PreguntaDicotomica()
    
    //arbol izq
    var p1112 : PreguntaDicotomica = PreguntaDicotomica()
    var p111 : PreguntaDicotomica = PreguntaDicotomica()
    var p11 : PreguntaDicotomica = PreguntaDicotomica()
    //arbol der
    var p1222 : PreguntaDicotomica = PreguntaDicotomica()
    var p1221 : PreguntaDicotomica = PreguntaDicotomica()
    var p122 : PreguntaDicotomica = PreguntaDicotomica()
    var p12 : PreguntaDicotomica = PreguntaDicotomica()
    
    //pregunta inicial
    var p1 : PreguntaDicotomica = PreguntaDicotomica()
    
    var preguntaActual : PreguntaDicotomica? = PreguntaDicotomica()


    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configureUI()
        definirPreguntas()
        showPregunta()
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueEspecieIdentificadaPorPreguntas" {
            let vc = segue.destination as! EspecieIdentificadaViewController
            vc.seleccionarEspecieDelegate = self.seleccionarEspecieDelegate
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1) {
            self.mainView.alpha = 1
        }
        configureUI()
        definirPreguntas()
        showPregunta()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func definirPreguntas(){
        //Definimos las preguntas finales
        // solo contemplamos 5 posibles caso
        pf1 = PreguntaDicotomica(pregunta: nil, respuesta1: nil, respuesta2: nil, esFinal: true, nuevaPregunta1: nil, nuevaPregunta2: nil, resultado: .misticeto)
        pf2 = PreguntaDicotomica(pregunta: nil, respuesta1: nil, respuesta2: nil, esFinal: true, nuevaPregunta1: nil, nuevaPregunta2: nil, resultado: .pinnipedo)
        pf3 = PreguntaDicotomica(pregunta: nil, respuesta1: nil, respuesta2: nil, esFinal: true, nuevaPregunta1: nil, nuevaPregunta2: nil, resultado: .sirenia)
        pf4 = PreguntaDicotomica(pregunta: nil, respuesta1: nil, respuesta2: nil, esFinal: true, nuevaPregunta1: nil, nuevaPregunta2: nil, resultado: .odontoceto)
        pf5 = PreguntaDicotomica(pregunta: nil, respuesta1: nil, respuesta2: nil, esFinal: true, nuevaPregunta1: nil, nuevaPregunta2: nil, resultado: .mustelido)
        
        
        //definimos el resto de las preguntas
        //arbol izq
        p1112 = PreguntaDicotomica(pregunta: "¿El espécimen tiene callosidades en la boca y arriba del ojo?", respuesta1: "Sí", respuesta2: "No", esFinal: false, nuevaPregunta1: pf1, nuevaPregunta2: pf4, resultado: nil)
        p111 = PreguntaDicotomica(pregunta: "¿El espécimen cuenta con líneas o canales por debajo de la boca?", respuesta1: "Sí, tiene pliegues en la piel", respuesta2: "No, la parte ventral es lisa", esFinal: false, nuevaPregunta1: pf1, nuevaPregunta2: p1112, resultado: nil)
        p11 = PreguntaDicotomica(pregunta: "¿Cuántos orificios respiratorios tiene el espécimen?", respuesta1: "Dos", respuesta2: "Uno", esFinal: false, nuevaPregunta1: p111, nuevaPregunta2: pf4, resultado: nil)
        
        //arbol derecho
        p1222 = PreguntaDicotomica(pregunta: "¿El espécimen tiene pelo?", respuesta1: "Sí", respuesta2: "No", esFinal: false, nuevaPregunta1: pf2, nuevaPregunta2: pf3, resultado: nil)
        p1221 = PreguntaDicotomica(pregunta: "¿El espécimen tiene cola larga?", respuesta1: "Sí", respuesta2: "No", esFinal: false, nuevaPregunta1: pf5, nuevaPregunta2: pf2, resultado: nil)
        p122 = PreguntaDicotomica(pregunta: "¿El espécimen tiene orejas?", respuesta1: "Sí", respuesta2: "No", esFinal: false, nuevaPregunta1: p1221, nuevaPregunta2: p1222, resultado: nil)
        p12 = PreguntaDicotomica(pregunta: "Su cabeza se parece a:", respuesta1: "Un delfín", respuesta2: "Un perro", esFinal: false, nuevaPregunta1: pf4, nuevaPregunta2: p122, resultado: nil)
        
        //Pregunta inicial
        p1 = PreguntaDicotomica(pregunta: "¿De qué tamaño es el animal?", respuesta1: "Más grande que un Beetle (vocho)", respuesta2: "Más chico que un Beetle (vocho)", esFinal: false, nuevaPregunta1: p11, nuevaPregunta2: p12, resultado: nil)
        
        
        preguntaActual = p1

    }
    
    func configureUI () {
        mainView.layer.cornerRadius = 10
        mainView.layer.shadowColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 1
        mainView.layer.shadowOpacity = 0.5
        mainView.layer.shouldRasterize = true
        mainView.layer.masksToBounds = false
        
        viewRespuesta2.layer.cornerRadius = 10
        
        
        
        viewRespuesta2.addTopBorder(color: UIColor.darkGray)
        viewRespuesta1.addTopBorder(color: UIColor.darkGray)
        
        let originalImage : UIImage = arrow1.image!
        let templateImage = originalImage.withRenderingMode(.alwaysTemplate)
        arrow1.image = templateImage
        arrow1.tintColor = UIColor.varaapp.yellow
        
        let originalImage2 : UIImage = arrow2.image!
        let templateImage2 = originalImage2.withRenderingMode(.alwaysTemplate)
        arrow2.image = templateImage2
        arrow2.tintColor = UIColor.varaapp.yellow
        
        
        
        viewRespuesta1.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.respuesta1Clic (_:))))
        viewRespuesta2.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.respuesta2Clic (_:))))
        
        btnReiniciar.layer.cornerRadius = btnReiniciar.frame.height / 2
        
        btnReiniciar.imageView?.setImageColor(color: .white)
        btnReiniciar.set(image: btnReiniciar.imageView?.image, title: "Reiniciar", titlePosition: .left, additionalSpacing: 12, state: .normal)
        
        
    }
    
    func showPregunta (){
        if let preguntaActual = self.preguntaActual {
            if(preguntaActual.esFinal){
                UIView.animate(withDuration: 1) {
                    self.mainView.alpha = 0
                }
                
                AvisoPorReportar.sharedInstance.tipoAnimal = preguntaActual.resultado!
                
                let alert = UIAlertController.init(title: "Especie identificada", message: "El especimen es: \(preguntaActual.resultado ?? .odontoceto)", preferredStyle: .alert)
                alert.addAction(title: "Continuar", style: .default) { (_) in
                    self.performSegue(withIdentifier: "segueEspecieIdentificadaPorPreguntas", sender: nil)
                }
                self.present(alert, animated: true, completion: nil)
                
            } else {
                lbPregunta.text = preguntaActual.pregunta
                lbRespuesta1.text = preguntaActual.respuesta1
                lbRespuesta2.text = preguntaActual.respuesta2
            }
        }
    }

    @objc func respuesta1Clic(_ sender:UITapGestureRecognizer){
        viewRespuesta1.gestureRecognizers?.forEach(viewRespuesta1.removeGestureRecognizer)
        UIView.animate(withDuration: 0.3, animations: {
            self.viewRespuesta1.backgroundColor = UIColor.varaapp.lightGreen
            
        }) { (_) in
            
            UIView.animate(withDuration: 0.8, animations: {
                self.viewRespuesta1.backgroundColor = UIColor.white
                self.hideLabels()
                self.preguntaActual = self.preguntaActual?.nuevaPregunta1
                
                
                
            }, completion: { (_) in
                self.showPregunta()
                self.viewRespuesta1.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.respuesta1Clic (_:))))
                UIView.animate(withDuration: 1, animations: {
                    self.revealLabels()
                })
                
            })
        }
        
    }
    @objc func respuesta2Clic(_ sender:UITapGestureRecognizer){
        viewRespuesta2.gestureRecognizers?.forEach(viewRespuesta2.removeGestureRecognizer)
        UIView.animate(withDuration: 0.3, animations: {
            self.viewRespuesta2.backgroundColor = UIColor.varaapp.lightGreen
            
        }) { (_) in
            
            UIView.animate(withDuration: 0.8, animations: {
                self.viewRespuesta2.backgroundColor = UIColor.white
                self.hideLabels()
                self.preguntaActual = self.preguntaActual?.nuevaPregunta2
                
            }, completion: { (_) in
                
                self.showPregunta()
                
                self.viewRespuesta2.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.respuesta2Clic (_:))))
                
                UIView.animate(withDuration: 1, animations: {
                    self.revealLabels()
                })
                
            })
        }
    }
    
    
    @IBAction func onReiniciaClic(_ sender: Any) {
        UIView.animate(withDuration: 0.7, animations: {
            self.mainView.alpha = 0
        }) { (_) in
            self.configureUI()
            self.definirPreguntas()
            self.showPregunta()
            UIView.animate(withDuration: 0.7, animations: {
                self.mainView.alpha = 1
            })
        }
        
    }
    func hideLabels (){
        lbPregunta.alpha = 0
        lbRespuesta1.alpha = 0
        lbRespuesta2.alpha = 0
        arrow1.alpha = 0
        arrow2.alpha = 0
    }
    func revealLabels (){
        lbPregunta.alpha = 1
        lbRespuesta1.alpha = 1
        lbRespuesta2.alpha = 1
        arrow1.alpha = 1
        arrow2.alpha = 1
    }
}
