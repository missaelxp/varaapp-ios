//
//  PerfilViewController.swift
//  varaapp
//
//  Created by Missael Hernandez on 28/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
class PerfilViewController: UITableViewController {

    
    @IBOutlet weak var lbNombreUsuario: UILabel!
    @IBOutlet weak var lbCorreo: UILabel!
    @IBOutlet weak var lbTelefono: UILabel!
    @IBOutlet weak var lbInstitucion: UILabel!
    @IBOutlet weak var switchWifi: UISwitch!
    @IBOutlet weak var switchKeepSession: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0)
        recuperarConfiguracion()
        loadPerfilFromServer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        recuperarConfiguracion()
        loadPerfilFromServer()
    }
    
    func recuperarConfiguracion(){
        switchWifi.isOn = Default.usarSoloPorWifi
        switchKeepSession.isOn = Default.mantenerSesionIniciada
    }
    
    @IBAction func onHomeClic(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadPerfilFromServer(){
        let tokenKC = KeychainWrapper.standard.string(forKey: Config.KeychainKeys.token)
        guard let token = tokenKC else {
            return
        }
        Services.obtenerUsuario(token: token, onComplete: { (informante) in
            DispatchQueue.main.async {
                self.lbNombreUsuario.text = "\(informante.nombre ?? "") \(informante.apellidoPaterno ?? "") \(informante.apellidoMaterno ?? "")"
                self.lbCorreo.text = informante.correoElectronico ?? ""
                self.lbInstitucion.text = informante.institucion ?? ""
                self.lbTelefono.text = informante.telefonoMovil ?? ""
                
            }
        }) { (error) in
            switch error {
            case .badRequest(_),
                 .serverSide,
                 .notFound,
                 .existingUser,
                 .timeOut:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "No fue posible conectar al servidor", message: "Revisa tu conexión e intenta de nuevo", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            case .unauthorized:
                self.solicitarInicioSesion()
            case .onlyWifiAllowed:
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Solo se permite el uso de red Wi-Fi", message: "Cambia la configuración en la sección de Perfil.", preferredStyle: .alert)
                    alert.addAction(title: "OK", style: .default, handler: nil)
                    self.present(alert, animated: true, completion: nil)
                    self.lbNombreUsuario.text = ""
                    self.lbCorreo.text = ""
                    self.lbInstitucion.text = ""
                    self.lbTelefono.text = ""
                }
            }
        }
    }
    
    func solicitarInicioSesion(){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "AuthScreens", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func onWifiSwitchChange(_ sender: Any) {
        Default.usarSoloPorWifi = self.switchWifi.isOn
    }
    
    @IBAction func onSwitchSessionChange(_ sender: Any) {
        Default.mantenerSesionIniciada = self.switchKeepSession.isOn
    }
    
    @IBAction func onCerrarSesionClic(_ sender: Any) {
        KeychainWrapper.standard.set("", forKey: Config.KeychainKeys.token)
        self.dismiss(animated: true, completion: nil)
        let storyboard = UIStoryboard(name: "AuthScreens", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController!.present(controller, animated: true, completion: nil)
        
    }
    
}
