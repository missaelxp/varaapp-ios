//
//  AppStatus.swift
//  varaapp
//
//  Created by Missael Hernandez on 24/09/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation

enum UsageStatus {
    case fueraDeLinea
    case logueado
}
class AppStatus {
    static let shared = AppStatus()
    private init (){
        
    }
    
    var usageStatus : UsageStatus = .fueraDeLinea
    var seHaIniciadoSesion : Bool = false
}
