//
//  Services.swift
//  varaapp
//
//  Created by Missael Hernandez on 20/07/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

enum NetworkError: Error {
    case serverSide
    case unauthorized
    case notFound
    case badRequest(errors : [String : Any])
    case existingUser
    case timeOut
    case onlyWifiAllowed
}


struct ApiEndpoint{
    //mauricio
    //https://varaweb.azurewebsites.net
    //localhost
    //public static let host = "192.168.1.8"
    public static let host = "varaweb.azurewebsites.net"
    //localhost
    //public static let baseURL = "https://\(host):44325"
    public static let baseURL = "https://\(host)"
    static let login = "\(baseURL)/api/Autenticacion/IniciarSesion"
    static let singup = "\(baseURL)/api/Autenticacion/RegistrarUsuario"
    static let verify = "\(baseURL)/api/Autenticacion/VerificarToken"
    static let obtenerUsuario = "\(baseURL)/api/Autenticacion/Usuario"
    static let obtenerVaramiento = "\(baseURL)/api/Aviso/Aviso"
    static let obtenerVaramientos = "\(baseURL)/api/Aviso/Avisos"
    static let registrarAviso = "\(baseURL)/api/Aviso/Reportar"
}
struct Services{
    
    public static let maxRequestSize = 19.8
    
    /// Verifica la conexión a red y verifica si el usuario ajustó las preferencias en "usar solo Wifi"
    ///
    /// - Returns: Retorna TRUE si está restringido el uso de red celular y la conexión actual es celular
    private static func checkWifiAndCurrentNetworkForRestriction() -> Bool{
        if ReachabilityManager.shared.reachability.connection == .cellular && Default.usarSoloPorWifi == true{
            return true
        } else {
            return false
        }
    }
    
    private static var manager: Alamofire.SessionManager = {
        
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "\(ApiEndpoint.host)": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    private static var tiny_manager: Alamofire.SessionManager = {
        
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "\(ApiEndpoint.host)": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 10
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    
    
    
    static func getImageFromServer (urlString: String,
                              onComplete completeHandler: @escaping (_ image: UIImage)-> Void,
                              onError errorHandler: @escaping (_ error: NetworkError) -> Void) -> Void {
        manager.request(urlString, method: .get).validate(statusCode: 200..<300).responseData { (responseData) in
            switch responseData.result {
            case .success(_):
                let image = UIImage(data: responseData.data!)!
                completeHandler(image)
            case .failure(_):
                errorHandler(NetworkError.serverSide)
            }
        }
    }
    
    
    static func iniciarSesion(correo: String, password:String,
                              onComplete completeHandler: @escaping (_ tokenRetrieved: String, _ expirationDate : String)-> Void,
                              onError errorHandler: @escaping (_ error: NetworkError) -> Void) -> Void {
        
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
        let parameters: Parameters = [
            "CorreoElectronico": correo,
            "Contraseña": password
        ]
        //manager
        tiny_manager.request(ApiEndpoint.login, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseJSON { (response) in
            switch response.result {
            case .success(_):
                if let responseCode = response.response?.statusCode{
                    switch responseCode{
                    case 200:
                        if let json = response.result.value {
                            let jsonObject : Dictionary = json as! Dictionary<String, Any>
                            
                            if  let token = jsonObject["token"] ,
                                let expiration = jsonObject["fecha_de_expiración"]{
                                completeHandler(token as! String, expiration as! String);
                            } else {
                                errorHandler(NetworkError.serverSide)
                            }
                        }
                    default:
                        errorHandler(NetworkError.serverSide)
                    }
                }  else {
                    errorHandler(NetworkError.serverSide)
                }
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    errorHandler(NetworkError.timeOut)
                } else {
                    if let responseCode = response.response?.statusCode{
                        switch responseCode{
                        case 400:
                            errorHandler(NetworkError.unauthorized)
                        case 404:
                            errorHandler(NetworkError.notFound)
                        default:
                            errorHandler(NetworkError.serverSide)
                        }
                    }  else {
                        errorHandler(NetworkError.serverSide)
                    }
                }
            }
            
            
            
        }
    }
    
    static func registrarUsuario(nombre: String, apellidoPaterno: String, apellidoMaterno: String, correo: String, password: String, passwordRepeat: String, telefono: String, institucion: String, onComplete completeHandler: @escaping (_ token: String, _ expirationDate : String) -> Void, onError errorHandler : @escaping (_ error: NetworkError) -> Void){
        
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
    
        var parameters: Parameters = [
            "Nombre": nombre,
            "TelefonoMovil": telefono,
            "CorrreoElectronico" : correo,
            "Contraseña": password,
            "ConfirmarContraseña": passwordRepeat
        ]
        if apellidoPaterno != "" {
            parameters.updateValue(apellidoPaterno, forKey: "ApellidoPaterno")
        }
        if apellidoMaterno != "" {
            parameters.updateValue(apellidoMaterno, forKey: "ApellidoMaterno")
        }
        
        if institucion != "" {
            parameters.updateValue(institucion, forKey: "Institucion")
        }

        
        
        
        manager.request(ApiEndpoint.singup, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseJSON { (response) in
            switch response.result {
            case .success:
                if let json = response.result.value {
                    let jsonObject : Dictionary = json as! Dictionary<String, Any>
                    
                    if  let token = jsonObject["token"] ,
                        let expiration = jsonObject["fecha_de_expiración"]{
                        completeHandler(token as! String, expiration as! String);
                    } else {
                        errorHandler(NetworkError.serverSide)
                    }
                } else {
                    errorHandler(NetworkError.serverSide)
                }
            case .failure:
                if let code = response.response?.statusCode {
                    switch code {
                    case 400:
                        if let data = response.data{
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                                if let jsonErrors = json {
                                    errorHandler(NetworkError.badRequest(errors: jsonErrors))
                                } else {
                                    errorHandler(NetworkError.serverSide)
                                }
                            } catch {
                                errorHandler(NetworkError.serverSide)
                            }
                        } else {
                            errorHandler(NetworkError.serverSide)
                        }
                    case 401:
                        errorHandler(NetworkError.unauthorized)
                    case 404:
                        errorHandler(NetworkError.notFound)
                    case 409:
                        errorHandler(NetworkError.existingUser)
                    default:
                        errorHandler(NetworkError.serverSide)
                    }
                } else {
                    errorHandler(NetworkError.serverSide)
                }
            }
        }
    
    }
    
    static func verificarToken(_ token : String, onComplete completeHandler: @escaping () -> Void, onError errorHandler : @escaping (_ error: NetworkError) -> Void){
        
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
        
        tiny_manager.request(ApiEndpoint.verify, method: .post,headers: headers)
            .validate(statusCode: 200..<300)
            .responseData { response in
                switch response.result {
                case .success:
                    completeHandler()
                case .failure(_):
                    switch response.response?.statusCode {
                    case 401:
                        errorHandler(NetworkError.unauthorized)
                    default:
                        errorHandler(NetworkError.serverSide)
                    }
                }
        }
    }
    
    static func obtenerUsuario(token:String, onComplete completeHandler: @escaping (_ informante : Informante) -> Void, onError errorHandler : @escaping (_ error: NetworkError) -> Void){
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
        manager.request(ApiEndpoint.obtenerUsuario, method: .get,headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result{
                case .success:
                    if let json = response.result.value {
                        let jsonObject : Dictionary = json as! Dictionary<String, Any>
                        
                        var informante : Informante = Informante()
                        informante.id = jsonObject["id"] as? Int
                        informante.nombre = jsonObject["nombre"] as? String
                        informante.correoElectronico = jsonObject["correoElectronico"] as? String
                        informante.apellidoPaterno = jsonObject["apellidoPaterno"] as? String
                        informante.apellidoMaterno = jsonObject["apellidoMaterno"] as? String
                        informante.institucion = jsonObject["institucion"] as? String

                        informante.telefonoMovil = jsonObject["telefonoMovil"] as? String

                        
                        completeHandler(informante)
                        
                    }
                case .failure(_):
                    guard let statusCode = response.response?.statusCode else {
                        errorHandler(NetworkError.serverSide)
                        return
                    }
                    switch statusCode {
                    case 400:
                        if let json = response.result.value {
                            let jsonObject : Dictionary = json as! Dictionary<String, Any>
                            errorHandler(NetworkError.badRequest(errors: jsonObject[""] as! Dictionary<String, Any>))
                        }
                    case 401:
                        errorHandler(NetworkError.unauthorized)
                    case 404:
                        errorHandler(NetworkError.notFound)
                    default:
                        errorHandler(NetworkError.serverSide)
                    }
                }
                
        }
    }
    
    static func obtenerVaramientos(token:String, onComplete completeHandler: @escaping (_ varamientos : [AvisoVaramiento]) -> Void, onError errorHandler : @escaping (_ error: NetworkError) -> Void){
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
        manager.request(ApiEndpoint.obtenerVaramientos, method: .get,headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                switch response.result{
                case .success:
                    if let json = response.result.value as! NSArray?{
                        var varamientosFromServer : [AvisoVaramiento] = [AvisoVaramiento]()
                        
                        for avisoRAW in json {
                            let jsonObject : Dictionary = avisoRAW as! Dictionary<String, Any>
                            var pushVaramiento = AvisoVaramiento()
                            
                            var dateString = jsonObject["fechaDeAvistamiento"] as? String
                            var date = dateString?.split(separator: "T")
                            dateString = String((date?[0])!)
                            let formatter = DateFormatter()
                            
                            
                            pushVaramiento.id = jsonObject["id"] as? Int
                            pushVaramiento.fecha = formatter.date(fromSwapiString: dateString!)
                            pushVaramiento.numeroEspecimenes = jsonObject["cantidadDeAnimales"] as? Int ?? 1
                            if jsonObject["fotografia"] as? String != nil {
                                pushVaramiento.fotografiasURLs.append(jsonObject["fotografia"] as! String)
                            }
                            
                            
                            varamientosFromServer.append(pushVaramiento)
                        }
                        
                        completeHandler(varamientosFromServer)
                    } else {
                        errorHandler(NetworkError.serverSide)
                    }
                case .failure(_):
                    if let code = response.response?.statusCode {
                        switch code {
                        case 400:
                            errorHandler(NetworkError.badRequest(errors: ["error": "400"]))
                        case 401:
                            errorHandler(NetworkError.unauthorized)
                        case 404:
                            errorHandler(NetworkError.notFound)
                        default:
                            errorHandler(NetworkError.serverSide)
                        }
                    } else {
                        errorHandler(NetworkError.serverSide)
                    }
                }
                
        }
    }
    
    static func obtenerVaramiento(token:String, idVaramiento: Int, onComplete completeHandler: @escaping (_ varamiento : AvisoVaramiento) -> Void, onError errorHandler : @escaping (_ error: NetworkError) -> Void){
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
        let param : [String: Any] = [
            "id" : idVaramiento
        ]
        manager.request(ApiEndpoint.obtenerVaramiento, method: .get, parameters : param, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                
                switch response.result{
                case .success:
                    if let jsonResponse = response.result.value as! Dictionary<String, Any>?{
                        guard let json = jsonResponse["aviso"] as! Dictionary<String, Any>? else {
                            errorHandler(NetworkError.serverSide)
                            return
                        }
                        var jsonFotos: [String] = []
                        if jsonResponse["fotografías"] != nil {
                            jsonFotos = jsonResponse["fotografías"] as! [String]
                        }
                        
                        var varamiento = AvisoVaramiento()
                        
                        varamiento.fotografiasURLs = jsonFotos
                        
                        var dateString = json["fechaDeAvistamiento"] as? String
                        var date = dateString?.split(separator: "T")
                        dateString = String((date?[0])!)
                        
                        let formatter = DateFormatter()
                        
                        varamiento.id = json["id"] as? Int
                        varamiento.fecha = formatter.date(fromSwapiString: dateString!)
                        varamiento.numeroEspecimenes = json["cantidadDeAnimales"] as? Int ?? 1
                        varamiento.acantilado = json["acantilado"] as? Bool ?? true
                    
                        let latitud = json["latitud"] as? Double
                        let longitud = json["longitud"] as? Double
                        varamiento.coordenadas = CLLocationCoordinate2D(latitude: latitud!, longitude: longitud!)
                        varamiento.facilAcceso = json["facilAcceso"] as? Bool ?? true
                        varamiento.observaciones = json["observaciones"] as? String
                        let enumVistoPorPrimeraVez = json["lugarDondeSeVio"] as? Int
                        if let enumVistoPorPrimeraVez = enumVistoPorPrimeraVez {
                            varamiento.vistoPorPrimeraVez = LugarDondeSeVio.fromServerId(id: "\(enumVistoPorPrimeraVez)")
                        }
                        
                        let enumSustrato = json["sustrato"] as? Int
                        if let enumSustrato = enumSustrato {
                            varamiento.tipoSustrato = Sustrato.fromServerId(id: "\(enumSustrato)")
                        }
                        
                        let enumEspecie = json["tipoDeAnimal"] as? Int
                        if let enumEspecie = enumEspecie {
                            varamiento.tipoAnimal = TipoAnimal.fromServerId(id: "\(enumEspecie)")
                        }
                        
                        
                        let enumEstadoAnimal = json["condicionDeAnimal"] as? Int
                        if let enumEstadoAnimal = enumEstadoAnimal {
                            varamiento.estadoAnimal = EstadoAnimal.fromServerId(id: "\(enumEstadoAnimal)")
                        }
                        
                        varamiento.nombreDelLugar = json["informacionDeLocalizacion"] as? String
                        
                        completeHandler(varamiento)
                    }
                case .failure(_):
                    if let code = response.response?.statusCode {
                        switch code {
                        case 401:
                            errorHandler(NetworkError.unauthorized)
                        default:
                            errorHandler(NetworkError.unauthorized)
                        }
                    } else {
                        errorHandler(NetworkError.serverSide)
                    }
                }
                
        }
    }
    
    static func enviarReporteDeVaramiento(token:String, onComplete completeHandler: @escaping () -> Void, onError errorHandler : @escaping (_ error: NetworkError) -> Void){
        if checkWifiAndCurrentNetworkForRestriction() {
            errorHandler(NetworkError.onlyWifiAllowed)
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
        
        var imagesData : [Data] = [Data]()
        
        for imagen in AvisoPorReportar.sharedInstance.fotografias{
            if let data = UIImageJPEGRepresentation(imagen, 0.5){
                imagesData.append(data)
            }
        }
        
        let formatter = DateFormatter()
        let dateString = formatter.string(fromSwapiDate: AvisoPorReportar.sharedInstance.fecha)
        
        
        
        
        
        let parameters:Dictionary<String, String> = [
            "Acantilado": "\(AvisoPorReportar.sharedInstance.acantilado)",
            "FacilAcceso": "\(AvisoPorReportar.sharedInstance.facilAcceso)",
            "Lugar": AvisoPorReportar.sharedInstance.vistoPorPrimeraVez.serverId,
            "Sustrato": AvisoPorReportar.sharedInstance.tipoSustrato.serverId,
            "FechaDeAvistamiento": dateString,
            "Observaciones": AvisoPorReportar.sharedInstance.observaciones,
            "TipoDeAnimal": AvisoPorReportar.sharedInstance.tipoAnimal.serverId,
            "CondicionDeAnimal": AvisoPorReportar.sharedInstance.estadoAnimal.serverId,
            "CantidadDeAnimales": "\(AvisoPorReportar.sharedInstance.numeroEspecimenes)",
            "Latitud": "\(AvisoPorReportar.sharedInstance.coordenadas.latitude)",
            "Longitud": "\(AvisoPorReportar.sharedInstance.coordenadas.longitude)",
            "InformacionDeLocalizacion" : "\(AvisoPorReportar.sharedInstance.nombreDelLugar). \(AvisoPorReportar.sharedInstance.indicaciones). \(AvisoPorReportar.sharedInstance.referenciasLugar)"
            
        ]
        
        
        manager.upload( multipartFormData: { multipartFormData in
            
            // import image to request
            for imageData in imagesData {
                multipartFormData.append(imageData, withName: "fotografias", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: ApiEndpoint.registrarAviso, method: .post, headers: headers,
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let code = response.response?.statusCode{
                        switch code {
                        case 200:
                            completeHandler()
                        case 400:
                            if let err = response.result.value as? Dictionary<String, Any>{
                                let errors : [String:Any] = err
                                errorHandler(NetworkError.badRequest(errors: errors))
                            } else {
                                errorHandler(NetworkError.serverSide)
                            }
                        case 401:
                            errorHandler(NetworkError.unauthorized)
                        case 404:
                            errorHandler(NetworkError.notFound)
                        default:
                            errorHandler(NetworkError.serverSide)
                        }
                    } else {
                        errorHandler(NetworkError.serverSide)
                    }
                }
            case .failure(_):
                errorHandler(NetworkError.badRequest(errors: ["error":"Componer petición \"multipart\""]))
            }
            
        })
    }
}












