//
//  Defaults.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/25/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
struct Default {
    
    static var usarSoloPorWifi : Bool {
        get {
            return UserDefaults.standard.bool(forKey: "\(Default.ultimoUsuario).\(Config.UDKeys.usarSoloPorWifi)")
        }
        set (o) {
            
            UserDefaults.standard.set(o, forKey: "\(Default.ultimoUsuario).\(Config.UDKeys.usarSoloPorWifi)")
        }
    }
    static var mantenerSesionIniciada : Bool {
        get {
            return UserDefaults.standard.bool(forKey: "\(Default.ultimoUsuario).\(Config.UDKeys.mantenerSesion)")
        }
        set (o) {
            UserDefaults.standard.set(o, forKey: "\(Default.ultimoUsuario).\(Config.UDKeys.mantenerSesion)")
        }
    }
    static var ultimoUsuario : String {
        get {
            return UserDefaults.standard.string(forKey: Config.UDKeys.ultimoUsuario) ?? ""
        }
        set (o) {
            UserDefaults.standard.set(o, forKey: Config.UDKeys.ultimoUsuario)
            if UserDefaults.standard.value(forKey: "\(Default.ultimoUsuario).\(Config.UDKeys.mantenerSesion)") == nil {
                UserDefaults.standard.set(true, forKey: "\(Default.ultimoUsuario).\(Config.UDKeys.mantenerSesion)")
            }
            
        }
    }
    
    static var ultimoInicioSesion : Date? {
        get {
            return UserDefaults.standard.object(forKey: Config.UDKeys.ultimoInicioSesion) as? Date
        }
        set (o){
            UserDefaults.standard.set(o, forKey: Config.UDKeys.ultimoInicioSesion)
        }
    }
    
    
}
