//
//  UIColorVaraapp.swift
//  varaapp
//
//  Created by Missael Hernandez on 25/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    struct varaapp {
        static let blue = UIColor(rgb: 0x14719C)
        static let yellow = UIColor(rgb: 0xE7BC32)
        static let lightGreen = UIColor(rgb: 0x61ba9a)
        static let disabledLightGreen = UIColor(rgb: 0x559171)
        static let alertRed = UIColor(red: 250, green: 62, blue: 62)
        static let recomendacionesTitle = UIColor(rgb: 0x1fac82)
    }
}
