//
//  UIImageColorizedImegeExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 05/09/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
