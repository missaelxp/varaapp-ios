//
//  DateExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 29/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
extension Date {
    func getPrintFecha(format: String) -> String{
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        return dateFormatterPrint.string(from: self)
    }
}
