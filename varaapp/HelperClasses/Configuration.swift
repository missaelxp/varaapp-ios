//
//  Configuration.swift
//  varaapp
//
//  Created by Missael Hernandez on 25/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
struct Config {
    
    struct UDKeys {
        static let usarSoloPorWifi = "useByWifiOnly"
        static let mantenerSesion = "keepSessionAlive"
        static let ultimoUsuario = "lastUser"
        static let ultimoInicioSesion = "lastLogin"
    }
    
    struct KeychainKeys {
        static let token = "token"
    }
}
