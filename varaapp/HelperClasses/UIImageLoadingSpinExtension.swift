//
//  UIImageLoadingSpinExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/21/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

extension UIImageView {
    
    func loadingIndicator(_ show : Bool){
        let tag = 8084042
        if show {
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            indicator.color = UIColor.darkGray
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
