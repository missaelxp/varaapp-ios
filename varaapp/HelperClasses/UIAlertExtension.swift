//
//  UIAlertExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 01/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension to `UIAlertController` to allow direct adding of `UIAlertAction` instances
 */
extension UIAlertController {
    
    func addAction(title: String?, style: UIAlertActionStyle, handler: ((UIAlertAction) -> Void)?) {
        let alertAction = UIAlertAction(title: title, style: style, handler: handler)
        self.addAction(alertAction)
    }
    
}
