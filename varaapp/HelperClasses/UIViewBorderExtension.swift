//
//  UIViewBorderExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 29/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import UIKit
extension UIView {
    func addBottomBorder(color : UIColor) {
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    func addTopBorder(color: UIColor)
    {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        border.borderWidth = width
        self.layer.addSublayer(border)
    }
}

