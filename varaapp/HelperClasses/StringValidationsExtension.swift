//
//  StringValidationsExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/21/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
extension String {
    
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    /// Verifica si la cadena es una contraseña aceptada por el servidor de Varaapp. Los requerimientos son: Debe tener al menos una letra mayúscula, una letra minúscula, un número y con una longitud entre 6 y 30 caractéres.
    ///
    /// - Returns: TRUE si cumple los requerimientos, FALSE de otro modo.
    func isValidPassword() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidPhone() -> Bool {
        if self.count != 10 {return false}
        let permitedLetters = "1234567890"
        for letra in self{
            if !permitedLetters.contains(letra){
                return false
            }
        }
        return true
    }
    
    func isValidUserField() -> Bool {
        if self.count < 1 || self.count > 60 {return false}
        let permitedLetters = "abcdefghijklmnñopqrstuvwxyzáéíóúABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚ "
        for letra in self{
            if !permitedLetters.contains(letra){
                return false
            }
        }
        return true
    }
    
    
}

