//
//  DateFormatterExtensionJSONDates.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/9/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
extension DateFormatter {
    func date(fromSwapiString dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10"
        self.dateFormat = "yyyy-MM-dd"
        //self.timeZone = TimeZone(abbreviation: "UTC")
        //self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
    func string(fromSwapiDate date: Date) -> String {
        self.dateFormat = "yyyy-MM-dd"
        //self.timeZone = TimeZone(abbreviation: "UTC")
        //self.locale = Locale(identifier: "en_US_POSIX")
        return self.string(from: date)
    }
}
