//
//  UIImageViewDownloadFromURLExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 24/06/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func downloadedFrom(url: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        self.loadingIndicator(true)
        contentMode = mode
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            self.loadingIndicator(false)
            self.image = imageFromCache
            return
        }
        
        
        
        Services.getImageFromServer(urlString: url, onComplete: { (image) in
            DispatchQueue.main.async() {
                let imageToCache = image
                imageCache.setObject(imageToCache, forKey: url as AnyObject)
                self.loadingIndicator(false)
                self.image = image
            }
        }) { (_) in
            self.loadingIndicator(false)
            self.image = #imageLiteral(resourceName: "FOTO_reporte_sin_fotos")
        }
    }
}

// ======== USAGE
// imageView.downloadedFrom(link: "http://www.apple.com/euro/ios/ios8/a/generic/images/og.png")
