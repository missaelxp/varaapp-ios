//
//  CLLocationDegreesExtension.swift
//  varaapp
//
//  Created by Missael Hernandez on 03/08/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationDegrees{
    func representacionEnGrados(esLatitud : Bool) -> String {
        let orientacion : String
        if (esLatitud){
            orientacion = (self > 0) ? "N" :"S"
        } else {
            orientacion = (self > 0) ? "E" : "W"
        }
        
        let medida = abs(self)
        
        let grados : Int = Int(medida)
        let pre_minutos : Double = (medida - Double(grados)) * 60
        let minutos = Int(pre_minutos)
        let pre_segundos = (pre_minutos - Double(minutos)) * 60
        //let segundos = Int(pre_segundos)
        //let decimales = pre_segundos - Double(segundos)
        
        return "\(grados)º \(minutos)' \(String(format: "%.3f", pre_segundos))'' \(orientacion)"
    }
}
