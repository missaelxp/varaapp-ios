//
//  NetworkStatusListener.swift
//  varaapp
//
//  Created by Missael Hernandez on 20/09/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import Reachability
public protocol NetworkStatusListener : class {
    func networkStatusDidChange(status: Reachability.Connection)
}
