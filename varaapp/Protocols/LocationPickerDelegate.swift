//
//  LocationPickerDelegate.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/2/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
import CoreLocation
protocol LocationPickerDelegate{
    func onLocationPicked(_ position:CLLocationCoordinate2D)
    func didCancel()
}
