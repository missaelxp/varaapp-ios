//
//  LoginDelegate.swift
//  varaapp
//
//  Created by Missael Hernandez on 10/15/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import Foundation
protocol LoginDelegate {
    func onLogin ()
}
