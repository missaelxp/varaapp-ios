//
//  varaappTestsServices.swift
//  varaappTestsServices
//
//  Created by Missael Hernandez on 30/07/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import XCTest
import CoreLocation
@testable import varaapp


class varaappTestsServices: XCTestCase {
    
    var globalToken : String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImE4QGdtYWlsLmNvbSIsImp0aSI6ImFjYTFmYzE2LTg0MzAtNDM2Yy04ZDMwLTU2N2FhZjhmZmMwMSIsImV4cCI6MTU0MzcxNTg1NywiaXNzIjoidmFyYXdlYi5jb20iLCJhdWQiOiJ2YXJhd2ViLmNvbSJ9.9f6Vlm_vq60wTSefum2mLcg3krNqVZt4F3ch3eRuUew"
    var globalUser : String = "a9@gmail.com"
    var globalPassword : String = "Missael_1234"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testSingupService(){
        let expectation = XCTestExpectation(description: "Waiting response from server")
        
        Services.registrarUsuario(nombre: "Missael", apellidoPaterno: "Hernández", apellidoMaterno: "Rosado", correo: self.globalUser, password: self.globalPassword, passwordRepeat: self.globalPassword, telefono: "1393201234", institucion: "Universidad Veracruzana", onComplete: { (mensaje, _)  in
            print(mensaje)
            XCTAssert(mensaje != "")
            expectation.fulfill()
        }) { (mensaje) in
            print(mensaje)
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testLoginService() {
        let expectation = XCTestExpectation(description: "Waiting token from web")
        
        Services.iniciarSesion(correo: self.globalUser, password: self.globalPassword, onComplete: { (token, expiration) in
            self.globalToken = token
            print(token)
            print(expiration)
            XCTAssert(token != "")
            expectation.fulfill()
        }) { (mensaje) in
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 30.0)
    }
    
    
    
    func testVerificarToken(){
        let expectation = XCTestExpectation(description: "Waiting response from server")
        
    Services.verificarToken(self.globalToken, onComplete: {
            XCTAssert(true)
            expectation.fulfill()
        }) { (_) in
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 400.0)
    }
    
    func testObtenerUsuario(){
        let expectation = XCTestExpectation(description: "Waiting response from server")
        
        Services.obtenerUsuario(token: self.globalToken, onComplete: { (mensaje) in
            print(mensaje);
            XCTAssert(true)
            expectation.fulfill()
        }) { (_) in
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 400.0)
    }
    
    func testObtenerVaramientos(){
        let expectation = XCTestExpectation(description: "Waiting response from server")
        
        Services.obtenerVaramientos(token: self.globalToken, onComplete: { (mensaje) in
            print(mensaje);
            XCTAssert(true)
            expectation.fulfill()
        }) { (_) in
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 400.0)
    }
    
    func testObtenerVaramiento(){
        let expectation = XCTestExpectation(description: "Waiting response from server")
        
        Services.obtenerVaramiento(token: self.globalToken, idVaramiento: 29, onComplete: { (mensaje) in
            print(mensaje);
            XCTAssert(true)
            expectation.fulfill()
        }) { (_) in
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 400.0)
    }
    
    func testEnviarReporteVaramiento(){
        let imagenes : [UIImage] = [
             #imageLiteral(resourceName: "varamiento-1")
        ]
       
        AvisoPorReportar.sharedInstance.acantilado = true
        AvisoPorReportar.sharedInstance.facilAcceso = true
        AvisoPorReportar.sharedInstance.fecha = Date()
        AvisoPorReportar.sharedInstance.numeroEspecimenes = 2
        AvisoPorReportar.sharedInstance.fotografias = imagenes
        AvisoPorReportar.sharedInstance.indicaciones = "Indicaciones son estas"
        AvisoPorReportar.sharedInstance.coordenadas = CLLocationCoordinate2D(latitude: 92.13983982, longitude: 92.13983982)
        AvisoPorReportar.sharedInstance.nombreDelLugar = "Nombre del lugar :)"
        AvisoPorReportar.sharedInstance.observaciones = "Las observaciones del lugar :)"
        AvisoPorReportar.sharedInstance.referenciasLugar = "Las referencias"
        AvisoPorReportar.sharedInstance.tipoSustrato = .arena
        AvisoPorReportar.sharedInstance.tipoAnimal = .misticeto
        AvisoPorReportar.sharedInstance.estadoAnimal = .vivo
        AvisoPorReportar.sharedInstance.vistoPorPrimeraVez = .playa 
        
        
        
        let expectation = XCTestExpectation(description: "Waiting response from server")
        
        Services.enviarReporteDeVaramiento(token: self.globalToken, onComplete: {
            XCTAssert(true)
            expectation.fulfill()
        }) { (_) in
            XCTFail();
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 400.0)
    }
}
