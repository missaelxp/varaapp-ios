//
//  CoreDataTest.swift
//  varaappTestsServices
//
//  Created by Missael Hernandez on 12/09/18.
//  Copyright © 2018 Missael Hernandez. All rights reserved.
//

import XCTest
import CoreData
import varaapp

class CoreDataTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInsertarDatos() {
        let entity = NSEntityDescription.entity(forEntityName: "Varamiento", in: CoreDataManager.context)
        let newVaramiento = NSManagedObject(entity: entity!, insertInto: CoreDataManager.context)
        newVaramiento.setValue("missael", forKey: "nombreDelLugar")
        
        try! CoreDataManager.save()
        assert(true)
    }
    
    func testSaveImagesInVaramiento(){
        
        
        let varamientoNuevo = Varamiento(context: CoreDataManager.context)
        let fotoNueva = FotografiaVaramiento(context: CoreDataManager.context)
        fotoNueva.fotografia = UIImageJPEGRepresentation(#imageLiteral(resourceName: "varamiento-1"), 0.8)! as NSData
        varamientoNuevo.especie = "Especimen con foto"
        varamientoNuevo.addToFotografiasAgregadas(fotoNueva)
        
        
        
        try! CoreDataManager.save()
    }
    
    func testSearchInCoreData(){
        let varamientos : [Varamiento] = CoreDataManager.findAll("Varamiento") as! [Varamiento]
        for vara in varamientos {
            print(vara.especie!)
        }
        
        assert(true)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
